module gitlab.com/hansxcs/centipedes

go 1.15

require (
	cloud.google.com/go/storage v1.10.0
	github.com/NaySoftware/go-fcm v0.0.0-20190516140123-808e978ddcd2
	github.com/gin-gonic/gin v1.7.4
	github.com/go-kit/kit v0.11.0 // indirect
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang/protobuf v1.5.2
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/jinzhu/copier v0.3.2
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.3.0
	github.com/mailgun/mailgun-go/v4 v4.5.3
	github.com/nicksnyder/go-i18n/v2 v2.1.2
	github.com/pressly/goose v2.7.0+incompatible
	github.com/qustavo/dotsql v1.1.0
	github.com/robfig/cron v1.2.0 // indirect
	github.com/robfig/cron/v3 v3.0.0
	github.com/sendgrid/rest v2.6.4+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.10.0+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/speps/go-hashids v2.0.0+incompatible
	github.com/spf13/cobra v1.2.1
	github.com/stretchr/testify v1.7.0
	github.com/ziutek/mymysql v1.5.4
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.19.1
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	golang.org/x/text v0.3.6
	golang.org/x/tools v0.1.7 // indirect
	google.golang.org/genproto v0.0.0-20210602131652-f16073e35f0c
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)
