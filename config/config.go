package config

import (
	"io/ioutil"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	zlog "gitlab.com/hansxcs/centipedes/src/package/log"
)

// SetupEngine ...
func SetupEngine(env string) {
	configEnvironment(env)
}

// configEnvironment ...
func configEnvironment(env string) {
	//setup log to JSON
	gin.DefaultWriter = ioutil.Discard
	// checking docekrize mode
	if env == "dockerize" {
		gin.DefaultWriter = ioutil.Discard
		zlog.Info("Docker Development Engine Running")
		return
	}

	// load env
	err := godotenv.Load()
	if err != nil {
		zlog.Fatal("Error loading .env file", err)
		os.Exit(1)
	}
	err = os.Setenv("APP_ENV", env)
	if err != nil {
		zlog.Info("Cannot load APP_ENV")
	}

	// Switch Enviroment
	switch env {
	case "development":
		zlog.Info("Development Engine Running")
	case "staging":
		gin.SetMode(gin.ReleaseMode)
		zlog.Info("Staging Engine Running")
	case "production":
		gin.SetMode(gin.ReleaseMode)
		zlog.Info("Production Engine Running")
	default:
		zlog.Info("Unknown Enviroment Engine Running")
	}
}
