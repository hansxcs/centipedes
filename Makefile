proto:
	protoc --go_out=plugins=grpc:math math.proto
    protoc --go_opt=paths=source_relative --go_out=plugins=grpc:.  proto/example/example.proto
	protoc --proto_path=proto --proto_path=proto_vendor --go_opt=paths=source_relative --go_out=plugins=grpc:proto/. proto/example/*.proto  
	# https://www.protop.io/about
	protoc --go_opt=paths=source_relative  --go_out=plugins=grpc:. proto_vendor/google/protobuf/empty.proto
gen:
	protoc --proto_path=proto proto/*.proto --go_out=plugins=grpc:.

test:
	go test -v ./src/domain/usecase/...
	# go test -v ./...

test-coverage:
	go test -coverprofile=coverage.out ./src/domain/usecase/user
	go tool cover -html=coverage.out