
declare START=$(date +%s)
declare -a PROTO_PACKAGES=("proto/common" "proto/health" "proto/status" "proto/example" "proto")
# proto-generator: generate grospin proto file
proto-generator(){
    protoc --version
    echo "  >  Start Generate Proto..."
    for package in ${PROTO_PACKAGES[@]}; do \
        protoc --proto_path=proto --proto_path=proto_vendor --go_opt=paths=source_relative --go_out=plugins=grpc:proto/. ${package}/*.proto 
        # protoc generator.$${package}.proto --proto_path=proto/merchant/generator/ --go_out=plugins=grpc:proto; \
    done
    echo "  >  Done Generate Proto..."
}
proto-generator

declare END=$(date +%s)
runtime=$(($END-$START))
echo "Process took $runtime seconds"
