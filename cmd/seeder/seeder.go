package seeder

import (
	"database/sql"
	"fmt"
	"net/url"
	"os"
	"time"

	"github.com/qustavo/dotsql"
	"github.com/spf13/cobra"
	"gitlab.com/hansxcs/centipedes/database/seeder"
	"gitlab.com/hansxcs/centipedes/src/package/log"
	"gitlab.com/hansxcs/centipedes/src/utils/helper"
)

// Serve ...
func Serve() *cobra.Command {
	return &cobra.Command{
		Use:   "seeder",
		Short: "database seeder",
		Run: func(c *cobra.Command, args []string) {
			Seeding(args)
		},
	}
}

// initDotSql ...
func initDotSql() (*sql.DB, *dotsql.DotSql, error) {
	defaultTimezone := os.Getenv("SERVER_TIMEZONE")
	configDB := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=1&loc=%s",
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASS"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_NAME"),
		url.QueryEscape(defaultTimezone),
	)
	db, err := sql.Open("mysql", configDB)
	if err != nil {
		return nil, nil, err
	}
	db.SetMaxIdleConns(helper.StringToInt(os.Getenv("DB_MAX_IDLE_CONN")))
	db.SetMaxOpenConns(helper.StringToInt(os.Getenv("DB_MAX_OPEN_CONN")))
	db.SetConnMaxIdleTime(time.Minute)
	db.SetConnMaxLifetime(time.Minute)

	// Loads queries from file
	dot, err := dotsql.LoadFromFile("./database/seeder/seeder.sql")
	if err != nil {
		return nil, nil, err
	}
	// dot := dotsql.Merge(dot1, dot2)
	return db, dot, nil
}

// Seeding ...
func Seeding(args []string) {
	db, dot, err := initDotSql()
	if err != nil {
		log.Fatal("failed to open connection db", err)
	}
	// Run queries
	allSeeds := seeder.ListSeeds(args)
	for _, element := range allSeeds {
		for query, eachSeed := range element {
			for _, v := range eachSeed.([][]interface{}) {
				if _, err := dot.Exec(db, query, v...); err != nil {
					errs := fmt.Sprintf("failed to seed: %s", query)
					log.Fatal(errs, err)
				}
			}
			fmt.Printf("Row Affected: %d\n", len(eachSeed.([][]interface{})))
			fmt.Println(query + ": OK")
		}
	}

	log.Info("seeding success")
}
