package cron

import (
	"log"

	"github.com/spf13/cobra"

	"gitlab.com/hansxcs/centipedes/src/adapter/mysql"
	cronPort "gitlab.com/hansxcs/centipedes/src/port/cron"
)

// CronJobServe ...
func CronJobServe() *cobra.Command {
	return &cobra.Command{
		Use:   "serveCron",
		Short: "Run Cron Job",
		Run: func(cmd *cobra.Command, args []string) {
			run()
		},
	}
}

func run() {
	db, err := mysql.Init()
	if err != nil {
		log.Panic(err)
	}
	conn := mysql.NewMySQL(db)
	service := cronPort.NewCronServer(conn)
	service.Job()
}
