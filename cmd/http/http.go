package http

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"gitlab.com/hansxcs/centipedes/src/adapter/mysql"
	"gitlab.com/hansxcs/centipedes/src/port/http"
)

var engine *gin.Engine

// Serve ...
func Serve() *cobra.Command {
	return &cobra.Command{
		Use:   "serveHTTP",
		Short: "Run HTTP Server",
		Run: func(cmd *cobra.Command, args []string) {
			Start()
		},
	}
}

// Start ...
func Start() {
	environment := flag.String("e", os.Getenv("APP_ENV"), "")
	flag.Usage = func() {
		fmt.Println("Usage: server -e {mode}")
		os.Exit(1)
	}
	flag.Parse()
	startApp(*environment)
}

func startApp(env string) {

	db, err := mysql.Init()
	if err != nil {
		log.Panic(err)
		os.Exit(1)
	}

	serverHost := os.Getenv("SERVER_ADDRESS")
	serverPort := os.Getenv("SERVER_PORT")
	serverString := fmt.Sprintf("%s:%s", serverHost, serverPort)

	conn := mysql.NewMySQL(db)

	httpServer := http.NewHttpServer(conn)
	httpServer.RoutesStart(serverString)

}
