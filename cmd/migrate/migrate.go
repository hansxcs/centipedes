package migrate

import (
	"log"
	"os"
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/cobra"
	database "gitlab.com/hansxcs/centipedes/src/adapter/mysql"
	"gitlab.com/hansxcs/centipedes/src/utils/helper"
)

// Serve ...
func Serve() *cobra.Command {
	return &cobra.Command{
		Use:   "migrate",
		Short: "database migration",
		Run: func(c *cobra.Command, args []string) {
			MigrateDatabase()
		},
	}
}

// MigrateDatabase ...
func MigrateDatabase() {
	// config not in dockerize mode
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file", nil)
		os.Exit(1)
	}
	database.DatabaseMigration(&database.Config{
		Host:         os.Getenv("DB_HOST"),
		Name:         os.Getenv("DB_NAME"),
		Password:     os.Getenv("DB_PASS"),
		Port:         helper.StringToInt(os.Getenv("DB_PORT")),
		User:         os.Getenv("DB_USER"),
		Timeout:      time.Duration(helper.StringToInt(os.Getenv("DB_TIMEOUT"))) * time.Second,
		MaxOpenConns: helper.StringToInt(os.Getenv("DB_MAX_OPEN_CONN")),
		MaxIdleConns: helper.StringToInt(os.Getenv("DB_MAX_IDLE_CONN")),
		MaxLifetime:  time.Duration(helper.StringToInt(os.Getenv("DB_MAX_LIFETIME"))) * time.Millisecond,
		Charset:      os.Getenv("DB_CHARSET"),
	})
}
