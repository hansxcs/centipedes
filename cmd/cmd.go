package cmd

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/hansxcs/centipedes/cmd/cron"
	"gitlab.com/hansxcs/centipedes/cmd/grpc"
	"gitlab.com/hansxcs/centipedes/cmd/http"
	"gitlab.com/hansxcs/centipedes/cmd/migrate"
	"gitlab.com/hansxcs/centipedes/cmd/seeder"
)

// Start handler registering service command
func Start() {
	rootCmd := &cobra.Command{}

	migrateCmd := migrate.Serve()
	migrateCmd.Flags().BoolP("version", "", false, "print version")
	migrateCmd.Flags().StringP("dir", "", "database/migration/", "directory with migration files")
	migrateCmd.Flags().StringP("table", "", "db", "migrations table name")
	migrateCmd.Flags().BoolP("verbose", "", false, "enable verbose mode")
	migrateCmd.Flags().BoolP("guide", "", false, "print help")

	seederCmd := seeder.Serve()

	cronJobCmd := cron.CronJobServe()

	httpCmd := http.Serve()
	httpCmd.Flags().StringP("environment", "e", "development", "-e environment")
	grpcCmd := grpc.Serve()
	grpcCmd.Flags().StringP("environment", "e", "development", "-e environment")

	cmd := []*cobra.Command{
		httpCmd,
		grpcCmd,
		cronJobCmd,
		migrateCmd,
		seederCmd,
	}

	rootCmd.AddCommand(cmd...)
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
