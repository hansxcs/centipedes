package grpc

import (
	"fmt"
	"net"
	"os"
	"os/signal"
	"runtime/debug"
	"syscall"

	"go.uber.org/zap"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/spf13/cobra"
	"gitlab.com/hansxcs/centipedes/src/package/log"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_zap "github.com/grpc-ecosystem/go-grpc-middleware/logging/zap"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	grpc_opentracing "github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	pb "gitlab.com/hansxcs/centipedes/proto"
	"gitlab.com/hansxcs/centipedes/src/adapter/mysql"
	"gitlab.com/hansxcs/centipedes/src/adapter/redis"

	example "gitlab.com/hansxcs/centipedes/src/port/grpc"
	"google.golang.org/grpc"
)

// Serve ...
func Serve() *cobra.Command {
	return &cobra.Command{
		Use:   "serveGRPC",
		Short: "serve GRPC",
		Run: func(c *cobra.Command, args []string) {
			run()
		},
	}
}

func run() {
	var logger *zap.Logger
	logger = log.InitLogger("production", "centipedes")

	db, err := mysql.Init()
	conn := mysql.NewMySQL(db)
	grpcServer := example.NewGRPCServer(conn)

	redisConn := redis.New()
	redisConn.Ping()

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGALRM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	grpcListener, err := net.Listen("tcp", ":50051")
	if err != nil {
		logger.Panic("failed listen", zap.Error(err))
		os.Exit(1)
	}

	go func() {
		baseServer := grpc.NewServer(
			grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
				grpc_recovery.StreamServerInterceptor(),
				grpc_ctxtags.StreamServerInterceptor(),
				grpc_opentracing.StreamServerInterceptor(),
				// grpc_prometheus.StreamServerInterceptor,
				// grpc_kit.StreamServerInterceptor(logger),
				grpc_zap.StreamServerInterceptor(logger),
				// grpc_auth.StreamServerInterceptor(myAuthFunction),
			)),
			grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
				grpc_recovery.UnaryServerInterceptor(grpc_recovery.WithRecoveryHandler(func(p interface{}) (err error) {
					debug.PrintStack()
					// logrus.Errorf("[PANIC] %s\n\n%s", p, string(debug.Stack()))
					e := logger
					msg := fmt.Sprintf("%v", p)

					e.DPanic("panic recoverable", zap.String("error panic", msg))
					return status.Errorf(codes.Internal, "panic triggered: %v", p)
				})),
				grpc_ctxtags.UnaryServerInterceptor(),
				grpc_opentracing.UnaryServerInterceptor(),
				// grpc_prometheus.UnaryServerInterceptor,
				// grpc_kit.UnaryServerInterceptor(logger),
				grpc_zap.UnaryServerInterceptor(logger),
				// grpc_auth.UnaryServerInterceptor(myAuthFunction),
			)),
		)
		pb.RegisterCompanyServiceServer(baseServer, grpcServer)
		logger.Info("success", zap.String("msg", "Server started successfully 🚀"))
		baseServer.Serve(grpcListener)
	}()
	logger.Error("error exit", zap.Any("exit", <-errs))
}
