-- +goose Up
-- +goose StatementBegin
CREATE TABLE `address_users` (
    `id` int(10)unsigned NOT NULL AUTO_INCREMENT,  
    `user_id` int(10)unsigned NOT NULL,
    `name` varchar(255) DEFAULT NULL,
    `detail` text NOT NULL,
    `created_at` datetime DEFAULT NULL,
    `updated_at` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_address_users_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS `address_users`;
-- +goose StatementEnd
