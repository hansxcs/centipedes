package seeder

import "fmt"

// AllSeed ...
type AllSeed struct {
	seeds []map[string]interface{}
}

// ListSeeds ...
func ListSeeds(args []string) []map[string]interface{} {
	allSeeds := &AllSeed{}
	allSeeds.seeds = []map[string]interface{}{}
	if len(args) < 1 {
		allSeeds.createOrderStatuses()
		allSeeds.createOrders()
	} else {
		switch args[0] {
		case "create-order-statuses":
			allSeeds.createOrderStatuses()
		case "create-orders":
			allSeeds.createOrders()
		default:
			fmt.Println("There is no `" + args[0] + "` seed found")
		}
	}
	return allSeeds.seeds
}
