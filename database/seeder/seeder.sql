--name: create-example
INSERT INTO example(`name`,`created_at`,`updated_at`) VALUES(?,?,?)

--name: create-roles
INSERT INTO roles(`id`,`name`,`display_name`,`description`,`created_at`,`updated_at`) VALUES(?,?,?,?,?,?)

--name: create-users
INSERT INTO users(`role_id`,`type`,`name`,`email`,`password`,`image`,`phone`,`created_at`,`updated_at`) VALUES(?,?,?,?,?,?,?,?,?)

--name: create-permissions
INSERT INTO permissions(`id`,`module`,`operations`,`created_at`,`updated_at`) VALUES(?,?,?,?,?) ON DUPLICATE KEY UPDATE `id`=`id`,`module`=`module`,`operations`=`operations`,`created_at`=`created_at`,`updated_at`=`updated_at`

--name: create-vehicle-categories
INSERT INTO vehicle_categories(`name`, `created_at`, `updated_at`) VALUES(?,?,?)

--name: create-vehicle-subcategories
INSERT INTO vehicle_subcategories(`vehicle_category_id`, `name`, `created_at`, `updated_at`) VALUES(?,?,?,?)

--name: create-vehicle-brands
INSERT INTO vehicle_brands(`name`, `created_at`, `updated_at`) VALUES(?,?,?)

--name: create-vehicle-models
INSERT INTO vehicle_models(`vehicle_brand_id`, `name`, `created_at`, `updated_at`) VALUES(?,?,?,?)

--name: create-vehicle-types
INSERT INTO vehicle_types(`vehicle_model_id`, `name`, `created_at`, `updated_at`) VALUES(?,?,?,?)

--name: create-vehicles
INSERT INTO vehicles(`vehicle_subcategory_id`, `vehicle_type_id`, `model_year`, `created_at`, `updated_at`) VALUES(?,?,?,?,?)

--name: create-role-permissions
INSERT INTO role_permissions(`role_id`,`module_id`,`permissions`) VALUES(?,?,?)

--name: create-user-vehicles
INSERT INTO user_vehicles(`user_id`,`vehicle_id`,`license_plate_number`, `odometer`, `created_at`, `updated_at`) VALUES(?,?,?,?,?,?)

--name: create-partners
INSERT INTO partners(`id`,`postal_code_id`,`name`,`address`,`latitude`,`longitude`,`time_offset`,`status`, `created_at`, `updated_at`) VALUES(?,?,?,?,?,?,?,?,?,?)

-- name: create-workshops
INSERT INTO workshops(`id`,`partner_id`,`postal_code_id`,`name`,`address`,`image`,`latitude`,`longitude`,`time_offset`,`maximum_service_capacity`,`average_rating`,`is_emergency`,`status`, `created_at`, `updated_at`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)

-- name: create-workshop-schedules
INSERT INTO workshop_schedules(`workshop_id`,`day`,`open_time`,`close_time`) VALUES(?,?,?,?)

--name: create-package-menus
INSERT INTO package_menus(`title`, `is_new`, `order`, `created_at`, `updated_at`) VALUES(?,?,?,?,?)

--name: create-banners
INSERT INTO banners(`title`, `start_at`, `end_at`, `status`, `created_at`, `updated_at`) VALUES(?,?,?,?,?,?)

--name: create-packages
INSERT INTO packages(`vehicle_category_id`, `name`, `description`, `image` ,`created_at`, `updated_at`) VALUES(?,?,?,?,?,?)

--name: create-emergencies
INSERT INTO emergencies(`id`, `customer_service_phone`, `ambulance_phone`) VALUES(?,?,?)

--name: create-faq-topics
INSERT INTO faq_topics(`id`, `name`, `status`, `created_at`, `updated_at`) VALUES(?,?,?,?,?) 

--name: create-faqs
INSERT INTO faqs(`id`, `faq_topic_id`,`question`,`answer`, `status`, `created_at`, `updated_at`) VALUES(?,?,?,?,?,?,?) 

--name: create-order-ratings
INSERT INTO order_ratings(`order_id`, `rating`, `comment`, `created_at`, `updated_at`) VALUES(?,?,?,?,?)

--name: create-rating-tags
INSERT INTO rating_tags(`name`, `created_at`, `updated_at`) VALUES(?,?,?)

--name: create-order-rating-tags
INSERT INTO order_rating_tags(`order_rating_id`, `rating_tag_id`, `created_at`, `updated_at`) VALUES(?,?,?,?)

--name: create-services
INSERT INTO services(`name`,`price`, `special_price`, `special_price_start_datetime`, `special_price_end_datetime`, `status`, `created_at`, `updated_at`) VALUES(?,?,?,?,?,?,?,?)

--name: create-compatible-vehicle-subcategories
INSERT INTO compatible_vehicle_subcategories(`service_id`,`subcategory_id`) VALUES(?,?)

--name: create-product-categories
INSERT INTO product_categories(`name`, `parent_category_id`, `status`, `created_at`, `updated_at`) VALUES(?,?,?,?,?)

--name: create-product-brands
INSERT INTO product_brands(`name`, `status`, `created_at`, `updated_at`) VALUES(?,?,?,?)

--name: create-products
INSERT INTO products(`brand_id`, `product_category_id`, `series`, `price`, `created_at`, `updated_at`) VALUES(?,?,?,?,?,?)

--name: create-compatible-vehicle-types
INSERT INTO compatible_vehicle_types(`product_id`, `vehicle_type_id`) VALUES(?,?)

--name: create-compatible-product-categories
INSERT INTO compatible_product_categories(`service_id`, `product_category_id`) VALUES(?,?)

--name: create-order-statuses
INSERT INTO order_statuses(`name`, `created_at`, `updated_at`) VALUES(?,?,?)

--name: create-vehicle-checklist-conditions
INSERT INTO vehicle_checklist_conditions(`name`, `created_at`, `updated_at`) VALUES(?,?,?)

--name: create-orders
INSERT INTO orders(`id`,`type`, `order_status_id`,`customer_id`,`customer_name`,`customer_phone`,`customer_address`,`customer_address_latitude`,`customer_address_longitude`,`customer_note`,`user_vehicle_id`, `vehicle_brand_name`,`vehicle_model_name`,`vehicle_type_name`,`vehicle_model_year`,`vehicle_name`,`vehicle_image`,`vehicle_license_plate_number`,`mechanic_id`,`mechanic_name`, `mechanic_phone`, `mechanic_note`,`workshop_id`,`workshop_partner_name`,`workshop_name`,`workshop_address`,`coupon_code_id`,`discount`,`book_datetime`,`service_scheduler_datetime`,`payment_datetime`,`created_at`,`updated_at`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)

--name: create-order-packages
INSERT INTO order_packages(`id`, `order_id`,`package_id`,`package_name`,`package_price`,`quantity`) VALUES(?,?,?,?,?,?)

--name: create-order-package-services
INSERT INTO order_package_services(`id`, `order_package_id`,`service_id`,`service_name`,`service_duration`,`service_price`,`quantity`) VALUES(?,?,?,?,?,?,?)

--name: create-order-package-products
INSERT INTO order_package_products(`id`, `order_package_service_id`,`product_id`,`product_name`,`product_price`,`mechanic_commission`,`quantity`) VALUES(?,?,?,?,?,?,?)

--name: create-package-services
INSERT INTO package_services(`service_id`,`package_id`, `is_mandatory`) VALUES(?,?,?)

--name: create-vehicle-checklists
INSERT INTO vehicle_checklists(`id`,`name`,`unit`, `is_note_fillable`, `created_at`, `updated_at`) VALUES(?,?,?,?,?,?)

--name: create-order-status-logs
INSERT INTO order_status_logs(`order_id`,`order_status_id`, `created_at`, `updated_at`) VALUES(?,?,?,?)

--name: create-coupons
INSERT INTO coupons(`id`, `name`,`quota`,`discount`, `discount_type`, `minimum_purchase`, `maximum_discount`,`order_start_datetime`, `order_end_datetime`, `use_start_datetime`,`use_end_datetime`,`description`, `terms_conditions`,`is_displayed`, `created_at`, `updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)

--name: create-coupon-codes
INSERT INTO coupon_codes(`id`, `code`,`coupon_id`, `used_quota`, `status`,`created_at`, `updated_at`) VALUES (?,?,?,?,?,?,?)

--name: create-coupon-products
INSERT INTO coupon_products(`coupon_id`, `product_id`) VALUES (?,?)

--name: create-coupon-packages
INSERT INTO coupon_packages(`coupon_id`, `package_id`) VALUES (?,?)

--name: create-coupon-users
INSERT INTO coupon_users(`coupon_code_id`, `user_id`) VALUES (?,?)

--name: create-coupon-user-vehicles
INSERT INTO coupon_user_vehicles(`coupon_code_id`, `user_vehicle_id`) VALUES (?,?)

--name: create-coupon-order-package-checklists
INSERT INTO order_package_checklists(`order_id`, `package_id`, `vehicle_checklist_id`, `vehicle_checklist_condition_id`,`note`, `is_verified`, `created_at`, `updated_at`) VALUES (?,?,?,?,?,?,?,?)

--name: create-coupon-packages
INSERT INTO vehicle_checklist_options(`vehicle_checklist_id`, `vehicles_checklist_condition_id`) VALUES (?,?)

--name: create-package-checklist
INSERT INTO package_checklists(`package_id`, `vehicle_checklist_id`) VALUES (?,?)

--name: create-unverified-service-books
INSERT INTO unverified_service_books(`vehicle_id`, `date`, `odometer`, `created_at`, `updated_at`) VALUES (?,?,?,?,?)

--name: create-unverified-service-book-services
INSERT INTO unverified_service_book_services(`service_id`, `unverified_service_book_id`, `other_service`) VALUES (?,?,?)

--name: create-order-payment-methods
INSERT INTO order_payment_methods(`order_id`, `payment_method`, `payment_method_display_name`, `amount`) VALUES(?,?,?,?)
--name: create-order-payment-method-logs
INSERT INTO order_payment_method_logs(`order_id`, `payment_method`, `log_datetime`) VALUES(?,?,?)