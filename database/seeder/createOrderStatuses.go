package seeder

import (
	"time"
)

func (s *AllSeed) createOrderStatuses() {
	now := time.Now()
	var seeds [][]interface{}

	seeds = append(seeds, []interface{}{"Pending", now, now})
	seeds = append(seeds, []interface{}{"Confirmed", now, now})
	seeds = append(seeds, []interface{}{"Canceled", now, now})
	seeds = append(seeds, []interface{}{"Expired", now, now})
	seeds = append(seeds, []interface{}{"In Transit", now, now})
	seeds = append(seeds, []interface{}{"In Progress", now, now})
	seeds = append(seeds, []interface{}{"Complete", now, now})
	seeds = append(seeds, []interface{}{"Paid", now, now})
	s.seeds = append(s.seeds, map[string]interface{}{
		"create-order-statuses": seeds,
	})
}
