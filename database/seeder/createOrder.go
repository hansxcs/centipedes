package seeder

import (
	"time"
)

func (s *AllSeed) createOrders() {
	now := time.Now()
	var seeds [][]interface{}

	seeds = append(seeds, []interface{}{"0008da82-f026-4f22-9cfc-56849f3ff4d6", 0, 1, 4, "customer", "8000000000", "", 0, 0, "", 2, "Yamaha", "Nmax", "ABS", "2018", "Yamaha Nmax ABS 2018", "", "B 4321 ABC", 5, "mechanic", "80000000001", "", 0, "Planet Ban", "Pasar Minggu", "No A, Jl. Raya Pasar Minggu No.25, RT.16/RW.6, Pejaten Tim., Kec. Ps. Minggu, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12510", nil, 0, now.AddDate(1, 30, 0), now.AddDate(1, 30, 0), nil, now, now})
	seeds = append(seeds, []interface{}{"0011a5bd-93b9-412a-831d-663e8eaae9e6", 0, 1, 4, "customer", "8000000000", "", 0, 0, "", 2, "Yamaha", "Nmax", "ABS", "2018", "Yamaha Nmax ABS 2018", "", "B 4321 ABC", 5, "mechanic", "80000000001", "", 0, "Planet Ban", "Pasar Minggu", "No A, Jl. Raya Pasar Minggu No.25, RT.16/RW.6, Pejaten Tim., Kec. Ps. Minggu, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12510", nil, 0, now.AddDate(1, 30, 0), now.AddDate(1, 30, 0), nil, now, now})
	seeds = append(seeds, []interface{}{"00a9ff69-cc30-439a-8abc-8082604cd16b", 0, 1, 4, "customer", "8000000000", "", 0, 0, "", 2, "Yamaha", "Nmax", "ABS", "2018", "Yamaha Nmax ABS 2018", "", "B 4321 ABC", 5, "mechanic", "80000000001", "", 0, "Planet Ban", "Pasar Minggu", "No A, Jl. Raya Pasar Minggu No.25, RT.16/RW.6, Pejaten Tim., Kec. Ps. Minggu, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12510", nil, 0, now.AddDate(1, 30, 0), now.AddDate(1, 30, 0), nil, now, now})
	seeds = append(seeds, []interface{}{"018002df-e47c-48ec-b70e-2baf391f88ac", 0, 1, 4, "customer", "8000000000", "", 0, 0, "", 2, "Yamaha", "Nmax", "ABS", "2018", "Yamaha Nmax ABS 2018", "", "B 4321 ABC", 5, "mechanic", "80000000001", "", 0, "Planet Ban", "Pasar Minggu", "No A, Jl. Raya Pasar Minggu No.25, RT.16/RW.6, Pejaten Tim., Kec. Ps. Minggu, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12510", nil, 0, now.AddDate(1, 30, 0), now.AddDate(1, 30, 0), nil, now, now})
	s.seeds = append(s.seeds, map[string]interface{}{
		"create-orders": seeds,
	})
}
