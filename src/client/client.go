package main

import (
	"context"
	"log"

	// "github.com/go-kit/kit/log"

	pb "gitlab.com/hansxcs/centipedes/proto"
	pbExample "gitlab.com/hansxcs/centipedes/proto/example"
	"google.golang.org/grpc"
)

func main() {
	var conn *grpc.ClientConn
	conn, err := grpc.Dial(":50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("some error appeared")

	}
	defer conn.Close()
	c := pb.NewCompanyServiceClient(conn)
	msg := &pbExample.CreateUserRequest{
		User: &pbExample.User{
			Name: "Hanzo",
		},
	}

	res, err := c.CreateUser(context.Background(), msg)
	if err != nil {
		log.Printf("%s, error when create user", res)
		log.Fatalf(err.Error())
	}
	log.Printf("Response from server: %s", res.GetUser())
}
