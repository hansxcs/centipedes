package dto

// Filter ...
type Filter struct {
	Limit  int      `json:"limit"`
	Page   int      `json:"page"`
	Query  *string  `json:"q"`
	Sort   string   `json:"sort"`
	Status int      `json:"status"`
	Order  []string `json:"order"`
}
