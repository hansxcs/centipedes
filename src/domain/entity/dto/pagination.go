package dto

// Pagination ...
type Pagination struct {
	Limit     int   `json:"limit"`
	Page      int   `json:"page"`
	TotalPage uint  `json:"total_page"`
	TotalRows int64 `json:"total_rows"`
}
