package dto

import "time"

type User struct {
	ID        uint       `json:"id"`
	Name      string     `json:"name"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
}

// UserRequest ...
type UserRequest struct {
	Name string `json:"name" form:"name" validate:"required,min=2,max=150"`
}

// UserResponse ...
type UserResponse struct {
	ID        string      `json:"id"`
	Name      string      `json:"name"`
	CreatedAt interface{} `json:"created_at"`
	UpdatedAt interface{} `json:"updated_at"`
}
