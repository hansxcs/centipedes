package user

import (
	"log"
	"time"

	"gitlab.com/hansxcs/centipedes/src/domain/entity/dao"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dto"
)

func (c *User) CreateUser(data *dto.User) error {
	now := time.Now()
	user := &dao.User{
		Name:      data.Name,
		CreatedAt: &now,
		UpdatedAt: &now,
	}
	if err := c.UserRepo.CreateUser(user); err != nil {
		log.Println(err.Error())
		return err
	}
	data.ID = user.ID
	data.Name = user.Name
	data.CreatedAt = user.CreatedAt
	data.UpdatedAt = user.UpdatedAt
	return nil
}
