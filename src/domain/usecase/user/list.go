package user

import (
	"log"

	"gitlab.com/hansxcs/centipedes/src/domain/entity/dao"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dto"
)

func (c *User) ListUsers(filter dto.Filter, paging *dto.Pagination, data *[]dto.User) error {
	rows, err := c.UserRepo.ListUsers(filter, paging, &[]dao.User{})
	if err != nil {
		log.Println(err)
		return err
	}

	res := []dto.User{}
	for _, user := range *rows {
		d := dto.User{
			ID:        user.ID,
			Name:      user.Name,
			CreatedAt: user.CreatedAt,
			UpdatedAt: user.UpdatedAt,
		}
		res = append(res, d)
		*data = append(*data, d)
	}

	return nil
}
