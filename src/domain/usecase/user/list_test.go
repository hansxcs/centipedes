package user

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dao"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dto"
	repo "gitlab.com/hansxcs/centipedes/src/domain/repository"
)

func TestService_ListUsers(t *testing.T) {

	users := &[]dto.User{}
	t.Run("Positive case", func(t *testing.T) {
		mock := new(repo.UserRepositoryMock)
		userService := User{UserRepo: mock}
		mock.On("ListUsers").Return(&[]dao.User{
			{ID: 1, Name: "A", CreatedAt: nil, UpdatedAt: nil},
			{ID: 2, Name: "B", CreatedAt: nil, UpdatedAt: nil},
			{ID: 3, Name: "C", CreatedAt: nil, UpdatedAt: nil},
		}, nil)
		userService.ListUsers(dto.Filter{}, &dto.Pagination{}, users)
		assert.Len(t, *users, 3)
	})

	t.Run("Negative case", func(t *testing.T) {
		mock := new(repo.UserRepositoryMock)
		userService := User{UserRepo: mock}
		mock.On("ListUsers").Return(&[]dao.User{}, errors.New("query syntax error"))
		err := userService.ListUsers(dto.Filter{}, &dto.Pagination{}, users)
		assert.NotEmpty(t, err, "error must be not empty")

	})

}
