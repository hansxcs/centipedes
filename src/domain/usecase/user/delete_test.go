package user

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	repo "gitlab.com/hansxcs/centipedes/src/domain/repository"
)

func TestService_DeleteUser(t *testing.T) {

	t.Run("Positive case", func(t *testing.T) {
		mock := new(repo.UserRepositoryMock)
		userService := User{UserRepo: mock}
		mock.On("DeleteUser").Return(nil)
		err := userService.DeleteUser(1)
		assert.Empty(t, err)
	})

	t.Run("Negative case", func(t *testing.T) {
		mock := new(repo.UserRepositoryMock)
		userService := User{UserRepo: mock}
		mock.On("DeleteUser").Return(errors.New("query syntax error"))
		err := userService.DeleteUser(1)
		assert.NotEmpty(t, err, "error must be not empty")

	})

}
