package user

import (
	"log"

	"gitlab.com/hansxcs/centipedes/src/domain/entity/dao"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dto"
	"gitlab.com/hansxcs/centipedes/src/utils/helper"
)

func (c *User) CheckUserDB() bool {
	filter := helper.QueryBuilder(map[string]string{
		"limit": "1",
		"page":  "1",
	})
	rows, err := c.UserRepo.ListUsers(*filter, &dto.Pagination{}, &[]dao.User{})
	if err != nil {
		log.Println(err.Error())
		return false
	}

	if len(*rows) > 0 {
		return true
	}

	return false
}
