package user

import (
	"log"
)

func (c *User) DeleteUser(id uint) error {
	if err := c.UserRepo.DeleteUser(id); err != nil {
		log.Println(err)
		return err
	}
	return nil
}
