package user

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hansxcs/centipedes/src/adapter/mysql"
)

func TestService_NewUser(t *testing.T) {

	conn, _ := mysql.Init()
	DB := mysql.NewMySQL(conn)
	defer conn.Close()
	t.Run("Positive case", func(t *testing.T) {
		userServices := New(DB)
		assert.NotEmpty(t, userServices)
	})

}
