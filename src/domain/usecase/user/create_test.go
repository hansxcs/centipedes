package user

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dto"
	repo "gitlab.com/hansxcs/centipedes/src/domain/repository"
)

func TestService_CreateUser(t *testing.T) {

	users := &dto.User{
		Name: "A",
	}
	t.Run("Positive case", func(t *testing.T) {
		mock := new(repo.UserRepositoryMock)
		userService := User{UserRepo: mock}
		mock.On("CreateUser").Return(nil)
		userService.CreateUser(users)
		assert.NotEmpty(t, users.ID)
	})

	t.Run("Negative case", func(t *testing.T) {
		mock := new(repo.UserRepositoryMock)
		userService := User{UserRepo: mock}
		mock.On("CreateUser").Return(errors.New("query syntax error"))
		err := userService.CreateUser(users)
		assert.NotEmpty(t, err, "error must be not empty")

	})

}
