package user

import (
	"log"

	"gitlab.com/hansxcs/centipedes/src/domain/entity/dao"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dto"
)

func (c *User) ReadUser(id uint, data *dto.User) error {
	user := &dao.User{}
	if err := c.UserRepo.ReadUser(id, user); err != nil {
		log.Println("Error get user", err)
		return err
	}
	data.ID = user.ID
	data.Name = user.Name
	data.CreatedAt = user.CreatedAt
	data.UpdatedAt = user.UpdatedAt
	return nil
}
