package user

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dto"
	repo "gitlab.com/hansxcs/centipedes/src/domain/repository"
)

func TestService_UpdateUser(t *testing.T) {

	users := &dto.User{
		Name: "B",
	}
	t.Run("Positive case", func(t *testing.T) {
		mock := new(repo.UserRepositoryMock)
		userService := User{UserRepo: mock}
		mock.On("UpdateUser").Return(nil)
		userService.UpdateUser(1, users)
		assert.NotEmpty(t, users.ID)
	})

	t.Run("Negative case", func(t *testing.T) {
		mock := new(repo.UserRepositoryMock)
		userService := User{UserRepo: mock}
		mock.On("UpdateUser").Return(errors.New("query syntax error"))
		err := userService.UpdateUser(1, users)
		assert.NotEmpty(t, err, "error must be not empty")

	})

}
