package user

import (
	"gitlab.com/hansxcs/centipedes/src/adapter/mysql"
	repo "gitlab.com/hansxcs/centipedes/src/domain/repository"
)

type User struct {
	UserRepo repo.UserInterface
}

func New(db *mysql.MySqlDB) *User {
	return &User{
		UserRepo: repo.New(db),
	}
}
