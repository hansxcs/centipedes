package usecase

import "gitlab.com/hansxcs/centipedes/src/domain/entity/dto"

type User interface {
	CreateUser(data *dto.User) error
	UpdateUser(id uint, data *dto.User) error
	ReadUser(id uint, data *dto.User) error
	ListUsers(data []*dto.User) error
	DeleteUser(id uint) error
	CheckUserDB() bool
}
