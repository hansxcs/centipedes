package repository

import (
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dao"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dto"
)

var (
	now = time.Now()
)

type UserRepositoryMock struct {
	mock.Mock
}

func (r *UserRepositoryMock) CreateUser(user *dao.User) error {

	args := r.Mock.Called()
	user.ID = 1
	user.CreatedAt = &now
	user.UpdatedAt = &now

	return args.Error(0)
}

func (r *UserRepositoryMock) UpdateUser(id uint, data map[string]interface{}, user *dao.User) error {
	args := r.Mock.Called()
	user.ID = id
	user.UpdatedAt = &now
	return args.Error(0)
}

func (r *UserRepositoryMock) ReadUser(id uint, user *dao.User) error {
	args := r.Mock.Called()
	user.ID = id
	user.Name = "Name Test"
	user.CreatedAt = &now
	user.UpdatedAt = &now
	return args.Error(0)
}

func (r *UserRepositoryMock) ListUsers(filter dto.Filter, paging *dto.Pagination, users *[]dao.User) (*[]dao.User, error) {
	args := r.Mock.Called()
	return args.Get(0).(*[]dao.User), args.Error(1)
}

func (r *UserRepositoryMock) DeleteUser(id uint) error {
	args := r.Mock.Called()
	return args.Error(0)
}
