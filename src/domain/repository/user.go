package repository

import (
	"fmt"

	"gitlab.com/hansxcs/centipedes/src/adapter/mysql"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dao"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dto"
	"gitlab.com/hansxcs/centipedes/src/package/log"
)

type User struct {
	mysql *mysql.MySqlDB
}

func New(db *mysql.MySqlDB) *User {
	return &User{
		mysql: db,
	}
}

type UserInterface interface {
	CreateUser(user *dao.User) error
	UpdateUser(id uint, data map[string]interface{}, user *dao.User) error
	ReadUser(id uint, user *dao.User) error
	ListUsers(filter dto.Filter, paging *dto.Pagination, users *[]dao.User) (*[]dao.User, error)
	DeleteUser(id uint) error
}

// CreateUser implements store.Store interface.
// This function stores the given user.
func (s *User) CreateUser(user *dao.User) error {
	queryInsert := &mysql.NewQueryBuilder{}
	queryInsert.Create("users", "name,created_at,updated_at")
	stringifyCreateQuery := fmt.Sprintf("%s", queryInsert.FullQuery)
	res := s.mysql.QueryRow(stringifyCreateQuery, user.Name, user.CreatedAt, user.UpdatedAt)
	id, _ := res.LastInsertId()
	querySelectNewRecord := &mysql.NewQueryBuilder{}
	querySelectNewRecord.
		Select("*").
		From("users").
		OperatorEqual("id", id)
	query := fmt.Sprintf("%s", querySelectNewRecord.FullQuery)
	if err := s.mysql.FetchRow(user, query); err != nil {
		log.Info(err.Error())
		return err
	}
	return nil
}

// UpdateUser implements store.Store interface.
// This function stores the given user.
func (s *User) UpdateUser(id uint, data map[string]interface{}, user *dao.User) error {
	queryUpdate := &mysql.NewQueryBuilder{}
	queryUpdate.
		Update("users", data).
		OperatorEqual("id", id)
	stringifyUpdateQuery := fmt.Sprintf("%s", queryUpdate.FullQuery)
	s.mysql.QueryRow(stringifyUpdateQuery)
	querySelectUpdatedRecord := &mysql.NewQueryBuilder{}
	querySelectUpdatedRecord.
		Select("*").
		From("users").
		OperatorEqual("id", id)
	query := fmt.Sprintf("%s", querySelectUpdatedRecord.FullQuery)
	if err := s.mysql.FetchRow(user, query); err != nil {
		log.Info(err.Error())
		return err
	}

	return nil
}

// ReadUser implements store.Store interface.
// This function stores the given user.
func (s *User) ReadUser(id uint, user *dao.User) error {
	qb := &mysql.NewQueryBuilder{}
	qb.
		Select("*").
		From("users").
		OperatorEqual("id", id)
	stringifySelectQuery := fmt.Sprintf("%s", qb.FullQuery)
	if err := s.mysql.FetchRow(user, stringifySelectQuery); err != nil {
		log.Info(err.Error())
		return err
	}

	return nil
}

// ListUser implements store.Store interface.
// This function stores the given user.
func (s *User) ListUsers(filter dto.Filter, paging *dto.Pagination, users *[]dao.User) (*[]dao.User, error) {
	qb := &mysql.NewQueryBuilder{}
	qb.
		Select("users.id,users.name,users.created_at,users.updated_at").
		From("users").
		OrderBy(filter.Order).
		Limit(filter.Limit).
		Offset(filter.Page)
	stringifySelectQuery := fmt.Sprintf("%s", qb.FullQuery)
	if err := s.mysql.Fetch(users, stringifySelectQuery); err != nil {
		log.Info(err.Error())
		return nil, err
	}
	return users, nil
}

// DeleteUserimplements store.Store interface.
// This function stores the given user.
func (s *User) DeleteUser(id uint) error {
	qb := &mysql.NewQueryBuilder{}
	qb.Delete("users", "id", id)
	stringifyDeleteQuery := fmt.Sprintf("%s", qb.FullQuery)
	s.mysql.QueryRow(stringifyDeleteQuery)
	return nil
}
