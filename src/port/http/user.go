package http

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/copier"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dto"
	"gitlab.com/hansxcs/centipedes/src/package/appctx"
	"gitlab.com/hansxcs/centipedes/src/package/log"
	"gitlab.com/hansxcs/centipedes/src/utils/helper"
	"gitlab.com/hansxcs/centipedes/src/utils/validator"
)

func (h *httpServer) ReadUser(c *gin.Context) *appctx.Result {
	userID, isError := helper.HashIDDecode(c.Param("id"))
	if isError {
		return &appctx.Result{
			Code:     http.StatusBadRequest,
			Messages: "User not found",
		}
	}

	user := &dto.User{}
	if err := h.UserService.ReadUser(userID, user); err != nil {
		log.Error("Error read user", err)
		return &appctx.Result{
			Code:     http.StatusBadRequest,
			Messages: "Error read user",
		}
	}

	if user.ID < 1 {
		log.Info("User not found")
		return &appctx.Result{
			Code:     http.StatusNotFound,
			Messages: "User not found",
		}
	}

	res := &dto.UserResponse{}
	copier.Copy(&res, &user)
	res.ID = helper.HashIDEncode(user.ID)
	res.CreatedAt = helper.ToJSONUnix(user.CreatedAt)
	res.UpdatedAt = helper.ToJSONUnix(user.UpdatedAt)
	return &appctx.Result{
		Code:     http.StatusOK,
		Status:   true,
		Messages: "Success",
		Detail:   res,
	}
}

func (h *httpServer) ListUsers(c *gin.Context) *appctx.Result {
	query := helper.QueryParser(c.Request.URL.Query())
	search := helper.QueryBuilder(query)
	paging := &dto.Pagination{
		Limit: search.Limit,
		Page:  search.Page,
	}

	users := &[]dto.User{}
	if err := h.UserService.ListUsers(*search, paging, users); err != nil {
		log.Error("Error read users", err)
		return &appctx.Result{
			Code:     http.StatusBadRequest,
			Messages: "Error read users",
		}
	}

	if len(*users) < 1 {
		log.Info("Users not found")
		return &appctx.Result{
			Code:     http.StatusNotFound,
			Messages: "Users not found",
		}
	}

	res := []dto.UserResponse{}
	copier.Copy(&res, &users)
	for i, val := range *users {
		copier.Copy(&res[i], &val)
		res[i].ID = helper.HashIDEncode(val.ID)
		res[i].CreatedAt = helper.ToJSONUnix(val.CreatedAt)
		res[i].UpdatedAt = helper.ToJSONUnix(val.UpdatedAt)
	}
	return &appctx.Result{
		Code:     http.StatusOK,
		Status:   true,
		Messages: "Success",
		Detail:   res,
	}
}

func (h *httpServer) CreateUser(c *gin.Context) *appctx.Result {
	var lang string
	if c.Value("PreferedLanguage") != nil {
		lang = c.Value("PreferedLanguage").(string)
	}
	payload := dto.UserRequest{}
	if err := c.ShouldBind(&payload); err != nil {
		return &appctx.Result{
			Code:     http.StatusBadRequest,
			Messages: "Bad Request",
		}
	}

	errs := validator.ValidateStruct(lang, payload)
	if errs != nil {
		return &appctx.Result{
			Code:     http.StatusBadRequest,
			Messages: strings.Join(errs[:], ". "),
			Errors:   errs,
		}
	}

	user := &dto.User{
		Name: payload.Name,
	}
	h.UserService.CreateUser(user)

	res := &dto.UserResponse{}
	copier.Copy(&res, &user)
	res.ID = helper.HashIDEncode(user.ID)
	res.CreatedAt = helper.ToJSONUnix(user.CreatedAt)
	res.UpdatedAt = helper.ToJSONUnix(user.UpdatedAt)
	return &appctx.Result{
		Code:     http.StatusOK,
		Status:   true,
		Messages: "Success",
		Detail:   res,
	}
}

func (h *httpServer) UpdateUser(c *gin.Context) *appctx.Result {
	userID, isError := helper.HashIDDecode(c.Param("id"))
	if isError {
		return &appctx.Result{
			Code:     http.StatusBadRequest,
			Messages: "User Not Found",
		}
	}

	var lang string
	if c.Value("PreferedLanguage") != nil {
		lang = c.Value("PreferedLanguage").(string)
	}
	payload := dto.UserRequest{}
	if err := c.ShouldBind(&payload); err != nil {
		return &appctx.Result{
			Code:     http.StatusBadRequest,
			Messages: "Bad Request",
		}
	}
	errs := validator.ValidateStruct(lang, payload)
	if errs != nil {
		return &appctx.Result{
			Code:     http.StatusBadRequest,
			Messages: strings.Join(errs[:], ". "),
			Errors:   errs,
		}
	}

	user := &dto.User{
		Name: payload.Name,
	}
	h.UserService.UpdateUser(userID, user)

	res := &dto.UserResponse{}
	copier.Copy(&res, &user)
	res.ID = helper.HashIDEncode(user.ID)
	res.CreatedAt = helper.ToJSONUnix(user.CreatedAt)
	res.UpdatedAt = helper.ToJSONUnix(user.UpdatedAt)
	return &appctx.Result{
		Code:     http.StatusOK,
		Status:   true,
		Messages: "Success",
		Detail:   res,
	}
}

func (h *httpServer) DeleteUser(c *gin.Context) *appctx.Result {
	userID, isError := helper.HashIDDecode(c.Param("id"))
	if isError {
		return &appctx.Result{
			Code:     http.StatusBadRequest,
			Messages: "User Not Found",
		}
	}

	if err := h.UserService.DeleteUser(userID); err != nil {
		log.Error("Error delete user", err)
		return &appctx.Result{
			Code:     http.StatusBadRequest,
			Messages: "Error delete user",
		}
	}

	return &appctx.Result{
		Code:     http.StatusOK,
		Status:   true,
		Messages: "Success",
		Detail:   "OK",
	}
}
