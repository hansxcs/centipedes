package http

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/hansxcs/centipedes/src/package/appctx"
)

type HTTPInterface interface {
	RoutesStart(serverString string)
	ReadUser(context *gin.Context) *appctx.Result
	ListUsers(context *gin.Context) *appctx.Result
	CreateUser(context *gin.Context) *appctx.Result
	UpdateUser(context *gin.Context) *appctx.Result
	DeleteUser(context *gin.Context) *appctx.Result
}
