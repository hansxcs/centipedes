package http

import (
	"gitlab.com/hansxcs/centipedes/src/adapter/mysql"
	"gitlab.com/hansxcs/centipedes/src/domain/usecase/user"
)

type httpServer struct {
	DB          *mysql.MySqlDB
	UserService *user.User
}

// NewHttpServer initializes a  gRPC server
func NewHttpServer(db *mysql.MySqlDB) HTTPInterface {
	return &httpServer{
		DB:          db,
		UserService: user.New(db),
	}
}
