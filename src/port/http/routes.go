package http

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/hansxcs/centipedes/src/package/appctx"
	"gitlab.com/hansxcs/centipedes/src/package/appctx/rest"
)

type httpPort func(c *gin.Context) *appctx.Result

func Handle(fn httpPort) gin.HandlerFunc {
	return func(c *gin.Context) {
		result := fn(c)
		rest.Result(c, result)
	}
}

func (n *httpServer) RoutesStart(serverString string) {
	// Creates a gin router with default middleware:
	// logger and recovery (crash-free) middleware
	router := gin.Default()

	router.GET("/user/:id", Handle(n.ReadUser))
	router.GET("/user", Handle(n.ListUsers))
	router.POST("/user", Handle(n.CreateUser))
	router.PUT("/user/:id", Handle(n.UpdateUser))
	router.DELETE("/user/:id", Handle(n.DeleteUser))

	// By default it serves on :8080 unless a
	// PORT environment variable was defined.
	router.Run(serverString)
}
