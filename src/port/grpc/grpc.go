package grpc

import (
	pb "gitlab.com/hansxcs/centipedes/proto"
	"gitlab.com/hansxcs/centipedes/src/adapter/mysql"
	"gitlab.com/hansxcs/centipedes/src/domain/usecase/user"
)

type gRPCServer struct {
	DB          *mysql.MySqlDB
	UserService *user.User
}

// NewGRPCServer initializes a new gRPC server
func NewGRPCServer(db *mysql.MySqlDB) pb.CompanyServiceServer {
	return &gRPCServer{
		DB:          db,
		UserService: user.New(db),
	}
}
