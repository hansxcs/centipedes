package grpc

import (
	"context"
	"log"

	"github.com/golang/protobuf/ptypes"
	pb "gitlab.com/hansxcs/centipedes/proto/example"
	pbStatus "gitlab.com/hansxcs/centipedes/proto/status"
	"gitlab.com/hansxcs/centipedes/src/domain/entity/dto"
	"gitlab.com/hansxcs/centipedes/src/utils/helper"
	statuscode "google.golang.org/genproto/googleapis/rpc/code"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

func (s *gRPCServer) CreateUser(ctx context.Context, req *pb.CreateUserRequest) (*pb.CreateUserResponse, error) {
	if ctx.Err() == context.Canceled {
		return nil, status.Error(codes.Canceled, "Request Cancelled ")
	}
	if ctx.Err() == context.DeadlineExceeded {
		return nil, status.Error(codes.DeadlineExceeded, "Deadline Exceeded")
	}
	data := dto.User{
		Name: req.User.Name,
	}
	// Create User Service
	if err := s.UserService.CreateUser(&data); err != nil {
		resErr := &pb.CreateUserResponse{
			Result: &pb.CreateUserResponse_Error{
				Error: &pbStatus.Status{
					Code:    int32(statuscode.Code_ABORTED),
					Message: "Error Internal Server",
					Details: []byte("test"),
				},
			},
		}
		return resErr, err
	}

	createdAt, _ := ptypes.TimestampProto(*data.CreatedAt)
	updatedAt, _ := ptypes.TimestampProto(*data.UpdatedAt)
	res := &pb.CreateUserResponse{
		Result: &pb.CreateUserResponse_User{
			User: &pb.User{
				Id:        helper.ToString(data.ID),
				Name:      data.Name,
				CreatedAt: createdAt,
				UpdatedAt: updatedAt,
			},
		},
	}

	log.Printf("Successfully Create New User")
	return res, nil
}

func (s *gRPCServer) UpdateUser(ctx context.Context, req *pb.UpdateUserRequest) (*pb.UpdateUserResponse, error) {
	if ctx.Err() == context.Canceled {
		return nil, status.Error(codes.Canceled, "Request Cancelled ")
	}
	if ctx.Err() == context.DeadlineExceeded {
		return nil, status.Error(codes.DeadlineExceeded, "Deadline Exceeded")
	}
	data := dto.User{
		Name: req.User.Name,
	}
	// Update User Service
	if err := s.UserService.UpdateUser(uint(helper.StringToInt(req.UserId)), &data); err != nil {
		resErr := &pb.UpdateUserResponse{
			Result: &pb.UpdateUserResponse_Error{
				Error: &pbStatus.Status{
					Code:    int32(statuscode.Code_ABORTED),
					Message: "Error Internal Server",
					Details: []byte("test"),
				},
			},
		}
		return resErr, err
	}

	createdAt, _ := ptypes.TimestampProto(*data.CreatedAt)
	updatedAt, _ := ptypes.TimestampProto(*data.UpdatedAt)
	res := &pb.UpdateUserResponse{
		Result: &pb.UpdateUserResponse_User{
			User: &pb.User{
				Id:        helper.ToString(data.ID),
				Name:      data.Name,
				CreatedAt: createdAt,
				UpdatedAt: updatedAt,
			},
		},
	}

	log.Printf("Successfully Update User")
	return res, nil
}

func (s *gRPCServer) ReadUser(ctx context.Context, req *pb.ReadUserRequest) (*pb.ReadUserResponse, error) {
	if ctx.Err() == context.Canceled {
		return nil, status.Error(codes.Canceled, "Request Cancelled ")
	}
	if ctx.Err() == context.DeadlineExceeded {
		return nil, status.Error(codes.DeadlineExceeded, "Deadline Exceeded")
	}
	data := dto.User{}
	// Create User Service
	if err := s.UserService.ReadUser(uint(helper.StringToInt(req.UserId)), &data); err != nil {
		resErr := &pb.ReadUserResponse{
			Result: &pb.ReadUserResponse_Error{
				Error: &pbStatus.Status{
					Code:    int32(statuscode.Code_ABORTED),
					Message: "Error Internal Server",
					Details: []byte("test"),
				},
			},
		}
		return resErr, nil
	}
	createdAt, _ := ptypes.TimestampProto(*data.CreatedAt)
	updatedAt, _ := ptypes.TimestampProto(*data.UpdatedAt)
	res := &pb.ReadUserResponse{
		Result: &pb.ReadUserResponse_User{
			User: &pb.User{
				Id:        helper.ToString(data.ID),
				Name:      data.Name,
				CreatedAt: createdAt,
				UpdatedAt: updatedAt,
			},
		},
	}

	log.Printf("Successfully Get User")
	return res, nil
}

func (s *gRPCServer) ListUsers(ctx context.Context, req *pb.ListUsersRequest) (*pb.ListUsersResponse, error) {
	if ctx.Err() == context.Canceled {
		return nil, status.Error(codes.Canceled, "Request Cancelled ")
	}
	if ctx.Err() == context.DeadlineExceeded {
		return nil, status.Error(codes.DeadlineExceeded, "Deadline Exceeded")
	}
	data := &[]dto.User{}
	paging := &dto.Pagination{}
	filter := helper.QueryBuilder(map[string]string{
		"q":     helper.PointerString(req.Key),
		"limit": req.Pagination.Limit,
		"page":  req.Pagination.Page,
		"sort":  req.Pagination.Sort,
	})
	// Create User Service
	if err := s.UserService.ListUsers(*filter, paging, data); err != nil {
		return nil, err
	}
	users := []*pb.User{}
	for _, user := range *data {
		createdAt, _ := ptypes.TimestampProto(*user.CreatedAt)
		updatedAt, _ := ptypes.TimestampProto(*user.UpdatedAt)
		user := &pb.User{
			Id:        helper.ToString(user.ID),
			Name:      user.Name,
			CreatedAt: createdAt,
			UpdatedAt: updatedAt,
		}
		users = append(users, user)

	}

	res := &pb.ListUsersResponse{
		Result: &pb.ListUsersResponse_Data{
			Data: &pb.ListUsersResponseOK{
				Users: users,
			},
		},
	}

	log.Printf("Successfully List User")
	return res, nil
}

func (s *gRPCServer) DeleteUser(ctx context.Context, req *pb.DeleteUserRequest) (*pb.DeleteUserResponse, error) {
	if ctx.Err() == context.Canceled {
		return nil, status.Error(codes.Canceled, "Request Cancelled ")
	}
	if ctx.Err() == context.DeadlineExceeded {
		return nil, status.Error(codes.DeadlineExceeded, "Deadline Exceeded")
	}
	// Delete User Service
	if err := s.UserService.DeleteUser(uint(helper.StringToInt(req.UserId))); err != nil {
		resErr := &pb.DeleteUserResponse{
			Result: &pb.DeleteUserResponse_Error{
				Error: &pbStatus.Status{
					Code:    int32(statuscode.Code_ABORTED),
					Message: "Error Internal Server",
					Details: []byte("test"),
				},
			},
		}
		return resErr, nil
	}

	res := &pb.DeleteUserResponse{
		Result: &pb.DeleteUserResponse_Empty{
			Empty: &emptypb.Empty{},
		},
	}

	log.Printf("Successfully Delete User")
	return res, nil
}
