package grpc

import (
	"context"

	"gitlab.com/hansxcs/centipedes/proto/health"
	empty "google.golang.org/protobuf/types/known/emptypb"
)

// Health implements accountproto.AccountServiceHandler interface
func (s *gRPCServer) Health(ctx context.Context, _ *empty.Empty) (*health.HealthResponse, error) {
	// Check database
	// if err := s.service.HealtsCheck(); err != nil {
	// 	res.Status = health.HealthResponse_NOT_SERVING
	// 	return err
	// }

	// // Check nats, i.e. call ourselves (exactly this node) via nats
	// if err := s.selfPingClient.Ping(ctx); err != nil {
	// 	res.Status = health.HealthResponse_NOT_SERVING
	// 	return errors.Wrapf(err, "unable to ping ourselves")
	// }

	return &health.HealthResponse{Status: health.HealthResponse_SERVING}, nil
}

// Ping implements accountproto.AccountServiceHandler and helath.Pinger interface.
// This is needed to implement self-pinger functionality.
func (s *gRPCServer) Ping(ctx context.Context, _ *empty.Empty) (*empty.Empty, error) {
	e := &empty.Empty{}
	return e, nil
}
