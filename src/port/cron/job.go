package cron

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/robfig/cron/v3"
)

// JobInterface ...
type JobInterface interface {
	Job()
}

func (s *cronServer) Job() {
	fmt.Println("Initiating cron job")
	jakartaTime, _ := time.LoadLocation("Asia/Jakarta")
	scheduler := cron.New(cron.WithLocation(jakartaTime))

	defer scheduler.Stop()

	scheduler.AddFunc("*/10 * * * *", func() {
		if s.UserService.CheckUserDB() {
			fmt.Println("User table exist")
		} else {
			fmt.Println("User table vanish or gone missing")
		}
	})
	scheduler.AddFunc("*/2 * * * *", func() { fmt.Println("Run each two minutes") })
	go scheduler.Start()

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	<-sig
}
