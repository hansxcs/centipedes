package cron

import (
	"gitlab.com/hansxcs/centipedes/src/adapter/mysql"
	"gitlab.com/hansxcs/centipedes/src/domain/usecase/user"
)

type cronServer struct {
	DB          *mysql.MySqlDB
	UserService *user.User
}

// NewCronServer initializes a new Cron server
func NewCronServer(db *mysql.MySqlDB) JobInterface {
	return &cronServer{
		DB:          db,
		UserService: user.New(db),
	}
}
