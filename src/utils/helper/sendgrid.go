package helper

import (
	"io/ioutil"
	"log"
	"os"
	"text/template"

	lo "gitlab.com/hansxcs/centipedes/src/package/log"
	"gitlab.com/hansxcs/centipedes/src/utils/i18n"

	"github.com/sendgrid/sendgrid-go"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

// SendGrid ...
type SendGrid struct {
	I18n *i18n.I18n
}

// SendGridHandler ...
func SendGridHandler() *SendGrid {
	return &SendGrid{
		I18n: i18n.NewHandler(),
	}
}

// SendGridInterface ...
type SendGridInterface interface {
	SendEmail(name string, email string, otp int, subject string, langCode string) bool
}

// SendEmail ...
func (s *SendGrid) SendEmail(name string, email string, otp int, subject string, langCode string) bool {

	s.I18n.ChangeLanguage(langCode, "")

	templateData := struct {
		OTP   int
		Body1 string
		Body2 string
		Body3 string
		Body4 string
	}{
		OTP:   otp,
		Body1: s.I18n.GetTranslation("email.forgot_phone.body_p1"),
		Body2: s.I18n.GetTranslation("email.forgot_phone.body_p2"),
		Body3: s.I18n.GetTranslation("email.forgot_phone.body_p3"),
		Body4: s.I18n.GetTranslation("email.forgot_phone.body_p4"),
	}

	tpl, err := template.ParseFiles("forgotPhone.html")
	if err != nil {
		lo.Error("Error Parsing HTML File", err)
	}

	tmpHTML, err := os.Create("./tmp/sendgrid.html")
	if err != nil {
		lo.Error("Error Creating HTML File", err)
	}

	defer tmpHTML.Close()
	if err := tpl.Execute(tmpHTML, templateData); err != nil {
		lo.Error("Error Execuiting Template", err)
	}

	from := mail.NewEmail("noreply", os.Getenv("SENDGRID_EMAIL"))
	to := mail.NewEmail(name, email)
	plainTextContent := templateData.Body1
	htmlContent, err := ioutil.ReadFile("./tmp/sendgrid.html")
	content := string(htmlContent)
	if err != nil {
		lo.Error("Error reading html file", err)
	}
	message := mail.NewSingleEmail(from, subject, to, plainTextContent, content)
	client := sendgrid.NewSendClient(os.Getenv("SENDGRID_API_KEY"))
	response, err := client.Send(message)
	if err != nil {
		log.Println(err)
	} else {
		log.Println(response.StatusCode)
		return true
	}

	return false
}
