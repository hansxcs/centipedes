package helper

import (
	"github.com/speps/go-hashids"
)

func initHasher() *hashids.HashID {
	hashConf := hashids.NewData()
	hashConf.Salt = ""
	hashConf.MinLength = 10 // Hash ID length, 8 means a hash ID such as xxxxxxxx
	hasher, _ := hashids.NewWithData(hashConf)
	return hasher
}

// HashIDEncode ...
func HashIDEncode(id uint) string {
	if id == 0 {
		return ""
	}
	h := initHasher()
	e, _ := h.Encode([]int{int(id)})
	return e
}

// HashIDDecode ...
func HashIDDecode(key string) (uint, bool) {
	h := initHasher()
	d, err := h.DecodeWithError(key)
	if err != nil {
		return 0, true
	}
	if len(d) == 0 {
		return 0, true
	}
	return uint(d[0]), false
}
