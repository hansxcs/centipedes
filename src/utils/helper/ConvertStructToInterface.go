package helper

import (
	"encoding/json"
	"reflect"
)

// ConvertStructToInterface ...
func ConvertStructToInterface(unchangedValue interface{}, changedValue interface{}) ([]string, *map[string]interface{}) {
	var jsonName []string
	a := changedValue
	changedValueReflect := reflect.ValueOf(changedValue).Elem()
	unchangedValueReflect := reflect.ValueOf(unchangedValue).Elem()
	for i := 0; i < changedValueReflect.NumField(); i++ {
		if changedValueReflect.FieldByName(changedValueReflect.Type().Field(i).Name).Interface() != unchangedValueReflect.FieldByName(unchangedValueReflect.Type().Field(i).Name).Interface() {
			jsonKey := changedValueReflect.Type().Field(i).Tag.Get("json")
			if jsonKey != "created_at" && jsonKey != "updated_at" {
				jsonName = append(jsonName, jsonKey)
			}
		}
	}

	var inInterface *map[string]interface{}
	inrec, _ := json.Marshal(a)
	json.Unmarshal(inrec, &inInterface)
	return jsonName, inInterface
}
