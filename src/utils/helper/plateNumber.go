package helper

import (
	"fmt"
	"regexp"
	"strings"
)

func PlateNumberValidator(num string) (string, error) {
	str := ""
	plate := strings.ReplaceAll(num, " ", "")
	isLetter := regexp.MustCompile(`^[a-zA-Z]+$`).MatchString
	isNumber := regexp.MustCompile("^[0-9]+$").MatchString

	vars := regexp.MustCompile(`\d+|\D+`).FindAllString(plate, -1)
	if len(vars) < 2 || len(vars) > 3 {
		return "", fmt.Errorf("error invalid plate number")
	}

	if !isLetter(vars[0]) || len(vars[0]) > 2 {
		return "", fmt.Errorf("error invalid plate number")
	}

	if !isNumber(vars[1]) || len(vars[1]) > 4 {
		return "", fmt.Errorf("error invalid plate number")
	}

	if len(vars) > 2 {
		if !isLetter(vars[2]) || len(vars[2]) > 3 {
			return "", fmt.Errorf("error invalid plate number")
		}
	}

	str = strings.Join(vars, " ")
	return str, nil

}
