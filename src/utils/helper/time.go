package helper

import (
	"strconv"
	"time"
	"log"
)

// GetLastStartSessionDay get date of monday session
func GetLastStartSessionDay() time.Time {
	now := time.Now()
	offset := int(time.Monday - now.Weekday())
	if offset > 0 {
		offset = -6
	}
	weekStartDate := time.Date(now.Year(), now.Month(), now.Day(), 10, 0, 0, 0, time.UTC).AddDate(0, 0, offset)
	if now.Before(weekStartDate) {
		weekStartDate = weekStartDate.AddDate(0, 0, -7)
	}
	return weekStartDate
}

func UnixToDate(unixString string) *time.Time {
	i, err := strconv.ParseInt(unixString, 10, 64)
	if err != nil {
		panic(err)
	}
	tm := time.Unix(i, 0)
	return &tm
}

func TimeTrack(start time.Time, name string) {
    elapsed := time.Since(start)
    log.Printf("%s took %s", name, elapsed)
}