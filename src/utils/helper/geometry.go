package helper

import "math"

// CalcDistanceGeo calculate distance 2 coordinate lat long
// @params: latitude ,Longitude,latitude2 ,Longitude2
func CalcDistanceGeo(lat float64, long float64, lat2 float64, long2 float64) uint {
	PI := 3.14159265358979323846264338327950288419716939937510582097494459
	EarthRadius := 6371
	la1, lo1 := lat*PI/180, long*PI/180
	la2, lo2 := lat2*PI/180, long2*PI/180
	deltaLat := la2 - la1
	deltaLong := lo2 - lo1
	a := math.Pow(math.Sin(deltaLat/2), 2) + math.Cos(la1)*math.Cos(la2)*math.Pow(math.Sin(deltaLong/2), 2)
	c := 2 * math.Asin(math.Sqrt(a))
	return uint(c * float64(EarthRadius) * 1000)
}
