package helper

import (
	"fmt"
	"math"
	"math/rand"
	"regexp"
	"strings"
	"time"
)

// GenerateRandomString params
// @length: int
// return string
func GenerateRandomString(length int) string {
	var seededRand *rand.Rand = rand.New(
		rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	const charset = "abcdefghijklmnopqrstuvwxyz" +
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// JoinStrings params
// @summary join string with array seperator [","]
// @str: []string
// return string
func JoinStrings(str []string) string {
	return fmt.Sprintf("[\"" + strings.Join(str, "\",\"") + "\"]")
}

// IsValidUUID params
// @summary check if UUID is valid with regex
// @uuid: string
// return bool
func IsValidUUID(uuid string) bool {
	r := regexp.MustCompile("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$")
	return r.MatchString(uuid)
}

// Slugify ...
func Slugify(str string) string {
	r := regexp.MustCompile(`^\s+|\s+$`)
	i := regexp.MustCompile(`[^a-z0-9 -]`)
	w := regexp.MustCompile(`\s+`)
	c := regexp.MustCompile(`-+`)
	str = r.ReplaceAllString(str, "")
	str = strings.ToLower(str)
	str = i.ReplaceAllLiteralString(str, "")
	str = w.ReplaceAllLiteralString(str, "-")
	str = c.ReplaceAllLiteralString(str, "-")

	return str
}

// StringPick ...
func StringPick(str string, min float64, max float64) string {
	var seededRand *rand.Rand = rand.New(
		rand.NewSource(time.Now().UnixNano()))
	var n int
	if max == 0 {
		n = int(min)
	} else {
		n = int(min + math.Floor((seededRand.Float64() * (max - min + 1))))
	}
	chars := make([]byte, n)
	for i := 0; i < n; i++ {
		chars[i] += str[int(math.Floor(seededRand.Float64()*float64(len(str))))]
	}
	return string(chars)
}

// StringShuffle ..
func StringShuffle(str string) string {
	array := strings.Split(str, "")
	if len(array) > 0 {
		r := rand.New(
			rand.NewSource(time.Now().UnixNano()))
		for i := range array {
			n := r.Intn(len(array))
			array[i], array[n] = array[n], array[i]
		}
	}
	return strings.Join(array[:], "")
}

// GenerateReferralCode ...
func GenerateReferralCode() string {
	const lowercase = "abcdefghijklmnopqrstuvwxyz"
	const numbers = "0123456789"
	key := ""
	const all = lowercase + numbers
	key += StringShuffle(StringPick(all, 8, 0))
	return key
}

// Reverse ...
func Reverse(str string) (result string) {
	for _, v := range str {
		result = string(v) + result
	}
	return
}

// ContainsList checks if a string is present in a slice
func ContainsList(str string, s []string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}
