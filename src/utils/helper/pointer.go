package helper

import "time"

// ComparePinterTime
// Input 2 data from *time.Time
// and the comparison is if a < b return true else false
func ComparePointerTime(time1 *time.Time, time2 *time.Time) bool {
	if time1 == nil || time2 == nil {
		return false
	}

	if time1.Unix() < time2.Unix() {
		return true
	}

	return false
}

// PointerString ...
func PointerString(str *string) string {
	if str != nil {
		return *str
	}
	return ""
}

// PointerUint ...
func PointerUint(value *uint) uint {
	if value != nil {
		return *value
	}
	return 0
}

// PointerFloat64 ...
func PointerFloat64(value *float64) float64 {
	if value != nil {
		return *value
	}
	return 0
}

func PointerBool(value *bool) bool {
	if value != nil {
		return *value
	}
	return false
}

func PointerInt(value *int) int {
	if value != nil {
		return *value
	}
	return 0
}
func PointerUintToPointerBool(value *uint) *bool {
	True := true
	False := false
	if value != nil {
		if PointerUint(value) == 1 {
			return &True
		} else {
			return &False
		}
	}

	return &False
}
