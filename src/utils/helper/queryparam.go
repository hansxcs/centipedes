package helper

import (
	"strconv"
	"strings"

	"gitlab.com/hansxcs/centipedes/src/domain/entity/dto"
)

// QueryBuilder ...
func QueryBuilder(query map[string]string) *dto.Filter {
	f := &dto.Filter{
		Limit:  10,
		Page:   1,
		Sort:   "id:desc",
		Status: 1,
	}
	for k, v := range query {
		if len(query[k]) > 0 && v != "" {
			if k == "limit" {
				limit, _ := strconv.ParseUint(v, 10, 32)
				f.Limit = int(limit)
			}
			if k == "page" {
				page, _ := strconv.ParseUint(v, 10, 32)
				if page <= 1 {
					page = 1
				}
				f.Page = int(page)
			}
			if k == "q" {
				v = strings.ReplaceAll(v, "+", " ")
				query := PointerString(&v)
				f.Query = &query
			}
			if k == "sort" {
				f.Sort = v
			}
			if k == "status" {
				status, _ := strconv.ParseUint(v, 10, 32)
				f.Status = int(status)
			}

		}
	}

	var order [2]string
	if f.Sort != "" {
		temp := strings.Split(f.Sort, ":")
		if order[0] = "id"; len(temp) > 0 {
			order[0] = temp[0]
			f.Order = append(f.Order, order[0])
		}
		if order[1] = "desc"; len(temp) > 1 {
			order[1] = temp[1]
			f.Order = append(f.Order, order[1])
		}
	}

	f.Page = (f.Page - 1) * f.Limit

	return f
}

// QueryParser
func QueryParser(query map[string][]string) map[string]string {
	parsed := map[string]string{}

	for k, v := range query {
		if len(v) > 0 {
			parsed[k] = v[0]
		}
	}

	return parsed
}
