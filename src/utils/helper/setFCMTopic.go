package helper

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"gitlab.com/hansxcs/centipedes/src/package/log"
)

// SetFCMTopic ...
func SetFCMTopic(registrationID string, topic string, fcmType string) bool {

	url := os.Getenv("FCM_URL_API") + "/v1"
	if fcmType == "subscribe" {
		url += ":batchAdd"
	} else {
		url += ":batchRemove"
	}

	var registration []string
	registration = append(registration, registrationID)

	jsonData := map[string]interface{}{
		"to":                  "/topics/" + topic,
		"registration_tokens": registration,
	}
	jsonValue, _ := json.Marshal(jsonData)

	request, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonValue))
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", "key="+os.Getenv("FCM_KEY"))
	client := &http.Client{}
	response, err := client.Do(request)
	fmt.Println(response)
	fmt.Println(err)
	if err != nil {
		log.Error("Error set fcm topic", err)
		return false
	} else if response != nil {
		return true
	}

	return false
}
