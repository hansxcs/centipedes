package helper

import (
	"bytes"
	"io/ioutil"

	"github.com/gin-gonic/gin"
)

// RequestBody ...
func RequestBody(context *gin.Context, order interface{}) error {
	var bodyBytes []byte
	if context.Request.Body != nil {
		bodyBytes, _ = ioutil.ReadAll(context.Request.Body)
	}
	context.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	if err := context.Bind(order); err != nil {
		return err
	}
	return nil
}
