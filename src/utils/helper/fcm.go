package helper

import (
	"fmt"
	"os"

	// "google.golang.org/api/fcm/v1"
	"github.com/NaySoftware/go-fcm"
)

type payloadFCM struct {
	Condition    string
	To           string
	Priority     string
	Notification fcm.NotificationPayload
	Data         interface{}
}

// FcmMessage ...
func FcmMessage(message map[string]interface{}) bool {
	configKey := os.Getenv("FCM_KEY")
	if configKey == "" {
		return false
	}
	payload := &payloadFCM{}
	if message["to"] != nil {
		payload.To = message["to"].(string)
	}
	payload.Priority = "normal"
	if message["priority"] != nil {
		payload.Priority = message["priority"].(string)
	}
	if message["condition"] != nil {
		payload.Condition = message["condition"].(string)
	}
	if message["data"] != nil {
		payload.Data = message["data"]
	}
	if message["notification"] != nil {
		if message["notification"].(map[string]interface{})["title"] != nil {
			payload.Notification.Title = message["notification"].(map[string]interface{})["title"].(string)
		}
		if message["notification"].(map[string]interface{})["body"] != nil {
			payload.Notification.Body = message["notification"].(map[string]interface{})["body"].(string)
		}
	}
	c := fcm.NewFcmClient(configKey)
	c.NewFcmMsgTo(payload.To, payload.Data)
	c.SetPriority(payload.Priority)
	c.SetContentAvailable(true) // Mandatory for Silent Notif
	c.SetCondition(payload.Condition)
	if payload.Notification.Title != "" || payload.Notification.Body != "" {
		c.SetNotificationPayload(&payload.Notification)
	}
	status, err := c.Send()
	if err != nil {
		fmt.Println(err)
	}
	// status.PrintResults()
	if status.StatusCode != 200 {
		fmt.Println("Failed sent message:", status)
	} else {
		fmt.Println("Successfully sent message:", status)
	}

	return true
}
