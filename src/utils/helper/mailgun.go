package helper

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	mailgun "github.com/mailgun/mailgun-go/v4"
)

// MailgunTemplate ...
type MailgunTemplate struct {
	Sender     string
	Subject    string
	Body       string
	Recipient  string
	HTMLString string
}

// SendEmail mailgun
func (mt *MailgunTemplate) SendEmail() {
	domain := os.Getenv("MAILGUN_DOMAIN")
	apikey := os.Getenv("MAILGUN_API_KEY")
	mg := mailgun.NewMailgun(domain, apikey)

	sender := mt.Sender
	subject := mt.Subject
	body := mt.Body
	recipient := mt.Recipient

	// The message object allows you to add attachments and Bcc recipients
	message := mg.NewMessage(sender, subject, body, recipient)
	if mt.HTMLString != "" {
		message.SetHtml(mt.HTMLString)
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	// Send the message with a 10 second timeout
	resp, id, err := mg.Send(ctx, message)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("ID: %s Resp: %s\n", id, resp)
}
