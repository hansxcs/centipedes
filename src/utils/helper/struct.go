package helper

import (
	"fmt"
	"reflect"
)

// ZeroValuesHandler get map[string]interface{} if value of different field value
// compare 2 structs and return different field value only
// @note 2 structs must be same field and type
// @params original interface{},new interface{}
// @return map[string]interface{}
func ZeroValuesHandler(request interface{}) map[string]interface{} {
	A := reflect.ValueOf(request)
	// B := reflect.ValueOf(original).Elem()
	if A.Kind() == reflect.Ptr {
		A = A.Elem()
	}

	values := make(map[string]interface{}, A.NumField())
	for i := 0; i < A.NumField(); i++ {
		if A.Field(i).Kind() != reflect.Slice {
			tag, ok := A.Type().Field(i).Tag.Lookup("json")

			fmt.Println("A: ", A.Field(i).Interface())
			// fmt.Println("B: ", B.FieldByName(A.Type().Field(i).Name).Interface())
			if A.Field(i).Kind() == reflect.Ptr && ok {
				if A.Field(i).IsNil() {
					// values[tag] = B.FieldByName(A.Type().Field(i).Name).Interface()
				} else {
					values[tag] = A.Field(i).Interface()
				}
			}
			fmt.Println("json: ", tag)
		}
	}
	fmt.Println("result map: ", values)
	return values
}

// GetDifferentValueStruct get map[string]interface{} if value of different field value
// compare 2 structs and return different field value only
// @note 2 structs must be same field and type
// @params original interface{},new interface{}
// @return map[string]interface{}
func GetDifferentValueStruct(original interface{}, new interface{}) map[string]interface{} {
	A := reflect.ValueOf(original)
	B := reflect.ValueOf(new)

	values := make(map[string]interface{}, A.NumField())
	if A.Type() != B.Type() {
		return values
	}
	for i := 0; i < A.NumField(); i++ {
		if A.Type().Field(i).Name == "ID" {
			if B.Field(i).Interface() != A.Field(i).Interface() {
				break
			}
		}
		if A.Field(i).Kind() != reflect.Slice {
			if B.Field(i).Interface() != A.Field(i).Interface() {
				tag, ok := A.Type().Field(i).Tag.Lookup("json")
				if ok && tag != "created_at" && tag != "updated_at" {
					values[tag] = B.Field(i).Interface()
				}
			}
		}
	}
	return values
}
