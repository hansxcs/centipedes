package helper

import (
	"bytes"
	"encoding/base64"
	"errors"
	"io"
	"net/http"
	"os"
	"regexp"

	"gitlab.com/hansxcs/centipedes/src/package/cloudbucket"
)

// UploadImage ...
// return path, error
func UploadImage(base64image string, folder string, filename string) (*string, error) {
	var path *string
	var err error
	if os.Getenv("APP_ENV") == "local" {
		path, err = uploadImageLocal(base64image, folder, filename)
		if err != nil {
			return nil, err
		}
	} else {
		path, err = uploadImageGCS(base64image, folder, filename)
		if err != nil {
			return nil, err
		}
	}

	return path, nil
}

// uploadGCS store temp image
func uploadImageGCS(base64image string, folder string, filename string) (*string, error) {
	m := regexp.MustCompile(`^data:image\/\w+;base64,`)

	if len(base64image) == 0 {
		return nil, errors.New("Error empty base64 string")
	}

	imageBuffer := m.ReplaceAllString(base64image, "")

	unbased, err := base64.StdEncoding.DecodeString(imageBuffer)
	if err != nil {
		return nil, err
	}
	r := bytes.NewReader(unbased)

	pathGCS := folder + "/" + filename
	path, err := cloudbucket.HandleFileUploadToBucket(r, pathGCS)

	return &path, err
}

// uploadLocal store temp image
func uploadImageLocal(base64image string, folder string, filename string) (*string, error) {
	m := regexp.MustCompile(`^data:image\/\w+;base64,`)
	imageBuffer := m.ReplaceAllString(base64image, "")
	path := "public/tmp/" + folder + "/"
	err := CheckFolder(path)
	if err != nil {
		MakeDirs(path)
	}
	unbased, err := base64.StdEncoding.DecodeString(imageBuffer)
	if err != nil {
		return nil, err
	}
	r := bytes.NewReader(unbased)

	out, err := os.Create(path + filename)
	defer func() {
		cerr := out.Close()
		if err == nil {
			err = cerr
		}
	}()

	_, err = io.Copy(out, r)
	if err != nil {
		return nil, err
	}
	path = "/" + folder + "/" + filename
	return &path, nil
}

// FindURLImageTarget check key for slice
func FindURLImageTarget(targets string) bool {
	targetsRules := []string{"users", "category", "eo"}
	for _, i := range targetsRules {
		if i == targets {
			return true
		}
	}
	return false
}

// DownloadFileURL ...
func DownloadFileURL(URL, folder string, fileName string) (string, error) {
	//Get the response bytes from the url
	response, err := http.Get(URL)
	if err != nil {
		return "", err
	}
	path := "public/tmp/" + folder + "/"
	err = CheckFolder(path)
	if err != nil {
		MakeDirs(path)
	}
	defer response.Body.Close()

	if response.StatusCode != 200 {
		return "", errors.New("Received non 200 response code")
	}
	//Create a empty file
	file, err := os.Create(path + fileName)
	if err != nil {
		return "", err
	}
	defer file.Close()

	//Write the bytes to the fiel
	_, err = io.Copy(file, response.Body)
	if err != nil {
		return "", err
	}
	path = "public/tmp/" + folder + "/" + fileName

	return path, nil
}
