package helper

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
)

// CompareSignatureKey ...
func CompareSignatureKey(data string, signatureKey string, typeModule string) bool {
	var isValid bool
	if typeModule == "product" {
		firstFourthCharacters := data[:4]
		lastSecondCharacters := data[len(data)-2:]
		newServerTime := firstFourthCharacters + lastSecondCharacters
		serverTimeRev := Reverse(newServerTime)
		newData := []byte(serverTimeRev)
		encodedData := md5.Sum(newData)
		pass := hex.EncodeToString(encodedData[:])
		pass = fmt.Sprintf("%x", encodedData)

		// check signature key
		if signatureKey != pass {
			isValid = false
		} else {
			isValid = true
		}
	}
	return isValid
}
