package helper

import (
	"io/ioutil"
	"os"
	"text/template"

	lo "gitlab.com/hansxcs/centipedes/src/package/log"
)

// EmailTemplate ...
func EmailTemplate(htmlTemplate string, emailData map[string]interface{}) string {
	path := "public/tmp/emails/"
	templateData := emailData

	tpl, err := template.ParseFiles("datasource/emails/" + htmlTemplate + ".html")
	if err != nil {
		lo.Error("Error Parsing HTML File", err)
	}
	err = CheckFolder(path)
	if err != nil {
		MakeDirs(path)
	}
	tmpHTML, err := os.Create(path + "mailgun.html")
	if err != nil {
		lo.Error("Error Creating HTML File", err)
	}

	defer tmpHTML.Close()
	if err := tpl.Execute(tmpHTML, templateData); err != nil {
		lo.Error("Error Execuiting Template", err)
	}

	htmlContent, err := ioutil.ReadFile(path + "mailgun.html")
	if err != nil {
		lo.Error("Error reading html file", err)
	}
	return string(htmlContent)
}
