package helper

import (
	"os"
	"strings"
	"time"
)

// ToJSONUnix check and convert to unix
func ToJSONUnix(date *time.Time) interface{} {
	if date != nil {
		return date.Unix()
	}
	return nil
}

// ToJSONFloat64 check and convert to unix
func ToJSONFloat64(data *float64) *float64 {
	if data != nil {
		return data
	}
	return nil
}

// ToJSONImage check image
func ToJSONImage(pathImg string) string {
	if pathImg != "" {
		if strings.Contains(pathImg, "http://") || strings.Contains(pathImg, "https://") {
			return pathImg
		}
		if os.Getenv("APP_ENV") == "local" {
			return os.Getenv("PATH_IMG") + pathImg
		}
		return os.Getenv("PATH_IMG") + "/" + os.Getenv("GCS_BUCKET") + pathImg
	}
	return ""
}

// ToJSONDOB check dob
func ToJSONDOB(dob string) string {
	t, _ := time.Parse("2006-01-02T15:04:05Z", dob)
	return t.Format("2006-01-02")
}

// ToJSONVideo check image
func ToJSONVideo(pathVideo string) string {
	if pathVideo != "" {
		return os.Getenv("PATH_FILE") + pathVideo
	}
	return ""
}
