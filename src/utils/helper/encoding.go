package helper

import "encoding/json"

// BytesIntoInterface ...
func BytesIntoInterface(data []byte, v interface{}) error {
	err := json.Unmarshal(data, v)
	if err != nil {
		return err
	}
	return nil
}

// InterfacetoBytes ...
func InterfacetoBytes(v interface{}) ([]byte, error) {
	e, err := json.Marshal(v)
	if err != nil {
		return nil, err
	}
	return e, nil
}
