package helper

// import (
// 	"fmt"
// 	"os"

// 	"gorm.io/driver/sqlserver"
// 	"gorm.io/gorm"
// )

// // OpenConnMsSQL ...
// func OpenConnMsSQL() (*gorm.DB, error) {
// 	dbhost := os.Getenv("MSSQL_HOST")
// 	dbport := os.Getenv("MSSQL_PORT")
// 	dbuser := os.Getenv("MSSQL_USER")
// 	dbpass := os.Getenv("MSSQL_PASS")
// 	dbname := os.Getenv("MSSQL_DB")
// 	configDB := fmt.Sprintf("sqlserver://%s:%s@%s:%s?database=%s",
// 		dbuser,
// 		dbpass,
// 		dbhost,
// 		dbport,
// 		dbname,
// 	)
// 	msSqlDB, err := gorm.Open(sqlserver.Open(configDB), &gorm.Config{})
// 	if err != nil {
// 		return nil, err
// 	}
// 	return msSqlDB, nil
// }
