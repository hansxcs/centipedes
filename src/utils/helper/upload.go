package helper

import (
	"fmt"
	"io"
	"os"

	"gitlab.com/hansxcs/centipedes/src/package/cloudbucket"
)

func UploadFile(gcsfolder, filepath, filename string) (*string, error) {
	var path *string
	var err error

	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	if os.Getenv("APP_ENV") == "local" {
		path, err = uploadLocal(file, gcsfolder, filename)
		if err != nil {
			return nil, err
		}
	} else {
		path, err = uploadGCS(file, gcsfolder, filename)
		if err != nil {
			return nil, err
		}
	}

	return path, nil
}

// uploadGCS store temp image
func uploadGCS(file io.Reader, folder string, filename string) (*string, error) {

	gcsFname := folder + "/" + filename

	path, err := cloudbucket.HandleFileUploadToBucket(file, gcsFname)
	if err != nil {
		fmt.Println(err)
	}

	return &path, err
}

// uploadLocal store temp video
func uploadLocal(file io.Reader, folder string, filename string) (*string, error) {
	path := "public/tmp/" + folder + "/"

	text := ParsingSpecialStringAlphanumeric(filename)
	filename = "default-" + GenerateStringWithCharSet(10) + "-" + text
	err := CheckFolder(path)
	if err != nil {
		err = MakeDirs(path)
		if err != nil {
			return nil, err
		}
	}
	fileCreate, _ := os.Create(path + filename)
	defer fileCreate.Close()
	io.Copy(fileCreate, file)
	fileCreate.Sync()
	path = path + filename
	return &path, nil
}
