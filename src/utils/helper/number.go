package helper

import (
	"math/rand"
	"time"
)

// RandInt random min max value
func RandInt(min int, max int) int {
	rand.Seed(time.Now().UnixNano())
	return min + rand.Intn(max-min)
}

// GetDiscountPercentage ..
func GetDiscountPercentage(original float64, discounted float64) uint {
	return uint(((original - discounted) * 100) / original)
}
