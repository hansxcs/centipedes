package helper

import (
	"bufio"
	"encoding/base64"
	"fmt"
	"log"
	"math/rand"
	"os"
	"regexp"
	"strings"
	"time"
)

// CheckFolder function check folder
func CheckFolder(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return err
	}
	return nil
}

// MakeDirs fucntion create directory
func MakeDirs(path string) error {
	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		return err
	}
	return nil
}

// FileRemove Remove Files
func FileRemove(path string) error {
	err := os.Remove(path)
	if err != nil {
		return err
	}
	return nil
}

// ParsingSpecialStringAlphanumeric func
// @text: string
// return string
func ParsingSpecialStringAlphanumeric(text string) string {
	res := strings.ReplaceAll(text, " ", "-")
	res = strings.ToLower(res)
	return res
}

// ParsingSpecialStringForS3 func
// @text: string
// return string
func ParsingSpecialStringForS3(text string) string {
	reg, err := regexp.Compile("[^ a-zA-Z0-9]")
	if err != nil {
		log.Fatal(err)
	}
	res := reg.ReplaceAllString(text, "")
	res = strings.ReplaceAll(text, " ", "_")
	return res
}

// GenerateStringWithCharSet params
// @Length: int
// return string
func GenerateStringWithCharSet(length int) string {
	var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	const charset = "abcdefghijklmnopqrstuvwxyz" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// ConvertImageToBase64 convert local image to base64 string
func ConvertImageToBase64(folder string, filename string) string {
	imgFile, err := os.Open("public/tmp/" + folder + "/" + filename) // a local code image

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defer imgFile.Close()

	// create a new buffer base on file size
	fInfo, _ := imgFile.Stat()
	var size int64 = fInfo.Size()
	buf := make([]byte, size)

	// read file content into buffer
	fReader := bufio.NewReader(imgFile)
	fReader.Read(buf)

	// if you create a new image instead of loading from file, encode the image to buffer instead with png.Encode()

	// png.Encode(&buf, image)

	// convert the buffer bytes to base64 string - use buf.Bytes() for new image
	imgBase64Str := base64.StdEncoding.EncodeToString(buf)
	return imgBase64Str
}
