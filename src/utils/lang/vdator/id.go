package vdator

// id language
func id(key string) string {
	m := make(map[string]string)
	m["required"] = "\"%s\" wajib diisi"
	m["required_if"] = "\"%s\" tidak boleh kosong"
	m["required_with"] = "\"%s\" tidak boleh kosong"
	m["required_without"] = "\"%s\" tidak boleh kosong"
	m["required_without_all"] = "\"%s\" tidak boleh kosong"
	m["email"] = "Format \"%s\" yang Anda masukkan salah"
	m["password"] = "Gunakan kombinasi huruf atau angka tanpa simbol"
	m["phone"] = "Nomor telepon harus diawali +62 atau 0"
	m["unique"] = "\"%s\" harus memiliki nilai unik"
	m["numeric"] = "Nilai \"%s\" harus angka"
	m["len"] = "Masukan \"%s\" sebanyak %s karakter"
	m["min"] = "\"%s\" minimal %s karakter"
	m["max"] = "\"%s\" maksimal  %s karakter"
	m["gt"] = "Masukkan \"%s\" lebih dari %s"
	m["lt"] = "Masukkan \"%s\" kurang dari %s"
	m["gte"] = "Masukkan \"%s\" sebanyak %s"
	m["lte"] = "Masukkan \"%s\"  kurang dari %s"
	m["eqfield"] = "\"%s\" yang kamu masukkan tidak sama dengan \"%s\""
	m["oneof"] = "\"%s\" harus memiliki salah datu dari \"%s\""
	m["dob"] = "Umur harus lebih dari 6 tahun dan kurang dari 17 tahun"
	return m[key]
}
