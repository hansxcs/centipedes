package vdator

// en language
func en(key string) string {
	m := make(map[string]string)
	m["required"] = "\"%s\" is not allowed to be empty"
	m["required_if"] = "\"%s\" is not allowed to be empty"
	m["required_with"] = "\"%s\" is not allowed to be empty"
	m["required_without"] = "\"%s\" is not allowed to be empty"
	m["required_without_all"] = "\"%s\" is not allowed to be empty"
	m["email"] = "You entered an incorrect \"%s\""
	// m["password"] = "\"%s\" is not valid"
	m["phone"] = "Start number with +62 or 0"
	m["unique"] = "\"%s\" value must be unique"
	m["numeric"] = "\"%s\" value must be numeric"
	m["len"] = "Enter \"%s\" as much as n character %s"
	m["min"] = "\"%s\" please enter at least %s character"
	m["max"] = "\"%s\" please enter maximum %s character"
	m["gt"] = "Enter \"%s\" more than %s"
	m["lt"] = "Enter \"%s\" less than %s"
	m["gte"] = "Enter \"%s\" as much as %s or more"
	m["lte"] = "Enter \"%s\" less than %s"
	m["gtefield"] = "\"%s\" value must be greater than or equal to %s"
	m["ltefield"] = "\"%s\" value must be lower than or equal to %s"
	m["eqfield"] = "\"%s\" you entered does not match with \"%s\""
	m["oneof"] = "Data \"%s\" is invalid"
	m["password"] = "Combine letters and numbers, and without symbol"
	m["dob"] = "Age must be between 6 and 17 years old"
	return m[key]
}
