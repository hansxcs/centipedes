package i18n

import (
	"encoding/json"

	"github.com/nicksnyder/go-i18n/v2/i18n"
	"golang.org/x/text/language"
)

var (
	bundle *i18n.Bundle
)

// I18n ...
type I18n struct {
	LanguageCode string
	Accept       string
	Loc          *i18n.Localizer
}

// NewHandler ...
func NewHandler() *I18n {
	return &I18n{
		Loc: Init(),
	}
}

// Init ...
func Init() *i18n.Localizer {
	// Set Default language
	bundle = i18n.NewBundle(language.Indonesian)
	bundle.RegisterUnmarshalFunc("json", json.Unmarshal)
	// Load Json Translation Files
	bundle.MustLoadMessageFile("datasource/i18n/translation.en.json")
	bundle.MustLoadMessageFile("datasource/i18n/translation.id.json")
	// @params *bundle , lang from request , lang from header
	// i.Accept = ctx.Request.Header.Get("Accept-Language")
	return i18n.NewLocalizer(bundle, "")
}

// ChangeLanguage  ...
// @summary if lang "" than take accept
// @summary lang = request.languageCode
// @sumarry accept = Header("Accept-Language")
// @params lang string
// @params accept string
func (i *I18n) ChangeLanguage(lang string, accept string) {
	i.Loc = i18n.NewLocalizer(bundle, lang, accept)
}

// GetTranslation ge translation from i18n json
// @params messageKey string
// @params messageData map[string]interface{}
// @return string
func (i *I18n) GetTranslation(messageKey string) string {
	translation := i.Loc.MustLocalize(&i18n.LocalizeConfig{
		MessageID: messageKey,
	})
	return translation
}

// GetTranslationWithData ge translation from i18n json
// @params messageKey string
// @params messageData map[string]interface{}
// @params lang string = "" for default
// @return string
func (i *I18n) GetTranslationWithData(messageKey string, messageData map[string]interface{}, lang string) string {
	if lang != "" {
		i.Loc = i18n.NewLocalizer(bundle, lang, "")
	}
	translation := i.Loc.MustLocalize(&i18n.LocalizeConfig{
		MessageID:    messageKey,
		TemplateData: messageData,
	})
	return translation
}
