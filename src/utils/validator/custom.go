package validator

import (
	"regexp"
	"time"
	"unicode"

	"github.com/go-playground/validator/v10"
)

// registerCustomValidation ...
func registerCustomValidation(validate *validator.Validate) *validator.Validate {
	validate.RegisterValidation("password", passwordHandler)
	validate.RegisterValidation("phone", phoneHandler)
	validate.RegisterValidation("dob", dobHandler)
	return validate
}

//CUSTOM HANDLERS
// phoneHandler ...
func phoneHandler(fl validator.FieldLevel) bool {
	match, _ := regexp.MatchString(`^(\+?62|0)`, fl.Field().String())
	if !match {
		return false
	}
	rm := regexp.MustCompile(`^(\+?62|0)`)
	number := rm.ReplaceAllString(fl.Field().String(), "")
	isPhone := regexp.MustCompile(`^([0-9]{7,14})`).MatchString
	return isPhone(number)
}

// passwordHandler
func passwordHandler(fl validator.FieldLevel) bool {
	password := fl.Field().String()
	return CheckPassword(password)
}

// dobHandler
func dobHandler(fl validator.FieldLevel) bool {
	dob := fl.Field().String()
	return CheckDOB(dob)
}

// CheckPassword ...
func CheckPassword(password string) bool {
	var IsLetter = regexp.MustCompile(`^[a-zA-Z]*[0-9]+$`).MatchString
	if IsLetter(password) == false {
		if CheckStringAlphabet(password) == true {
			return true
		}
		return false
	}
	return true
}

// CheckDOB ...
func CheckDOB(dob string) bool {
	today := time.Now().Truncate(0)
	six := today.AddDate(-6, 0, 0)
	seventeen := today.AddDate(-17, 0, 0)
	t, _ := time.Parse("2006-01-02", dob)
	if t.Before(six) && t.After(seventeen) {
		return true
	}

	return false
}

// CheckStringAlphabet ...
func CheckStringAlphabet(str string) bool {

	var checkAplhabet [2]bool
	for _, charVariable := range str {
		if !unicode.IsLetter(charVariable) {
			if !unicode.IsNumber(charVariable) {
				return false
			}
		}
		if charVariable < 'a' || charVariable > 'z' {
			checkAplhabet[0] = true
		} else if charVariable < 'A' || charVariable > 'Z' {
			checkAplhabet[1] = true
		}
	}
	if checkAplhabet[0] == true && checkAplhabet[1] == true {
		return true
	}
	return false
}
