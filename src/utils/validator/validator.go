package validator

import (
	"bytes"
	"fmt"
	"strings"
	"unicode"

	"github.com/go-playground/validator/v10"
	"gitlab.com/hansxcs/centipedes/src/utils/lang/vdator"
)

var validate *validator.Validate

// ValidateStruct ...
func ValidateStruct(ln string, model interface{}) []string {
	validate = validator.New()
	validate = registerCustomValidation(validate)
	var errorMessage []string
	// returns nil or ValidationErrors ( []FieldError )
	err := validate.Struct(model)
	if err != nil {

		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println(err)
			return nil
		}

		for _, err := range err.(validator.ValidationErrors) {
			var errs string
			var buffer bytes.Buffer
			trimStruct := err.StructNamespace()[strings.Index(err.StructNamespace(), ".")+1:]
			var length = len(trimStruct) - 1

			for i, rune := range trimStruct {
				if unicode.IsUpper(rune) && i != 0 && i != length && trimStruct[i-1] != 46 { // 46 == .
					buffer.WriteRune('_')
				}
				buffer.WriteRune(rune)
			}
			underscored := buffer.String()
			tagError := strings.ToLower(underscored)
			switch err.Tag() {
			case "required":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError)
			case "required_if":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError)
			case "required_with":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError)
			case "required_without":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError)
			case "required_without_all":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError)
			case "email":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError)
			case "phone":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError)
			case "unique":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError)
			case "numeric":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError)
			case "len":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError, err.Param())
			case "min":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError, err.Param())
			case "max":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError, err.Param())
			case "gt":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError, err.Param())
			case "lt":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError, err.Param())
			case "gte":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError, err.Param())
			case "lte":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError, err.Param())
			case "gtefield":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError, err.Param())
			case "ltefield":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError, err.Param())
			case "eqfield":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError, err.Param())
			case "oneof":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()), tagError, err.Param())
			case "password":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()))
			case "dob":
				errs = fmt.Sprintf(vdator.New(ln, err.Tag()))
			}
			errorMessage = append(errorMessage, errs)
		}
		return errorMessage
	}
	return nil
}
