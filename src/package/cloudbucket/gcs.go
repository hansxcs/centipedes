package cloudbucket

import (
	"context"
	"io"
	"net/url"
	"os"

	"cloud.google.com/go/storage"
	"gitlab.com/hansxcs/centipedes/src/package/log"
)

// HandleFileUploadToBucket uploads file to bucket
func HandleFileUploadToBucket(file io.Reader, filename string) (string, error) {

	var storageClient *storage.Client

	bucket := os.Getenv("GCS_BUCKET")
	ctx := context.Background()

	var err error
	fname := ""
	if len(filename) != 0 {
		fname = filename
	}

	storageClient, err = storage.NewClient(ctx)
	if err != nil {
		log.Error(err.Error(), err)
		return "", err
	}

	sw := storageClient.Bucket(bucket).Object(fname).NewWriter(ctx)

	if _, err := io.Copy(sw, file); err != nil {
		log.Error(err.Error(), err)
		return "", err
	}

	if err := sw.Close(); err != nil {
		log.Error(err.Error(), err)
		return "", err
	}

	u, err := url.Parse("/" + bucket + "/" + sw.Attrs().Name)
	if err != nil {
		log.Error(err.Error(), err)
		return "", err
	}

	path := u.EscapedPath()

	return path, nil
}
