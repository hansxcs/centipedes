package appctx

import (
	"gitlab.com/hansxcs/centipedes/src/package/appctx/pagination"
)

// Result ...
type Result struct {
	Name       string
	Status     bool
	Messages   string
	Code       int
	ErrorCode  uint
	Errors     interface{}
	Result     interface{}
	Detail     interface{}
	List       interface{}
	Meta       interface{}
	Pagination *pagination.Response
}
