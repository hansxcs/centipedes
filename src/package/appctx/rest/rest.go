package rest

import (
	"encoding/json"
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/hansxcs/centipedes/src/package/appctx"
	"gitlab.com/hansxcs/centipedes/src/package/appctx/pagination"
)

// MetaResponse types
type MetaResponse struct {
	Error   uint   `json:"error"`
	Code    int    `json:"code"`
	Status  bool   `json:"status"`
	Message string `json:"message"`
}

// APIResponse types
type APIResponse struct {
	Meta  *MetaResponse `json:"meta,omitempty"`
	Data  interface{}   `json:"data"`
	Error interface{}   `json:"error"`
}

// Detail ...
type Detail struct {
	Detail interface{} `json:"detail"`
}

// List  ...
type List struct {
	List       interface{}          `json:"list"`
	Pagination *pagination.Response `json:"pagination,omitempty"`
}

// DetailList ...
type DetailList struct {
	Detail     interface{}          `json:"detail"`
	List       interface{}          `json:"list"`
	Pagination *pagination.Response `json:"pagination,omitempty"`
}

// Data ...
type Data struct {
	Counter    interface{}          `json:"counter"`
	List       interface{}          `json:"list"`
	Pagination *pagination.Response `json:"pagination,omitempty"`
}

// Result ...
// func (r *Rest) Result(context *gin.Context, result *apps.Result) {
func Result(context *gin.Context, result *appctx.Result) {
	var data interface{}
	if result != nil {
		if result.Detail != nil && result.List != nil {
			data = DetailList{result.Detail, result.List, result.Pagination}
		} else if result.Detail != nil {
			data = Detail{
				result.Detail,
			}
		} else if result.List != nil {
			data = List{
				result.List,
				result.Pagination,
			}
		} else {
			data = result.Result
		}
		response := APIResponse{
			Meta: &MetaResponse{
				Error:   result.ErrorCode,
				Code:    result.Code,
				Status:  result.Status,
				Message: result.Messages,
			},
			Data:  data,
			Error: result.Errors,
		}

		if os.Getenv("LOGGING_ENV") == "development" {
			meta, err := json.Marshal(response.Meta)
			if err != nil {
				panic(err)
			}

			data, err := json.Marshal(data)
			if err != nil {
				panic(err)
			}

			errr, err := json.Marshal(response.Error)
			if err != nil {
				panic(err)
			}

			log.Println(`{"level":"info","message":"Response Data", "response":{"meta":` + string(meta) + "," + `"data":` + string(data) + "," + `"error":` + string(errr) + "}}")
		}

		context.JSON(result.Code, response)
	}
}
