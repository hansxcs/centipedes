package appctx

const (
	NoError uint = 0

	/*
		Will return 1, if:
		- partner try to login but its status is inactive
		- customer try to login but its status is inactive
		- any attempts to access all endpoints that require either customer or partner level
	*/
	UserInactive uint = 1

	/*
		Will return 2, if:
		- partner try to change status order to in progress from inactive customer
		- any nitrogen related endpoints in partner app for inactive customer
	*/
	CustomerInactive uint = 2

	/*
		Will return 2, if:
		- token is expired
	*/
	TokenExpired uint = 3

	/*
		Will return 10, if:
		- There is an error after query to database
	*/
	ErrorQuery uint = 10
)

var errorText = map[uint]string{
	NoError:          "No Error",
	UserInactive:     "User inactive. Please try again or contact our administrator",
	CustomerInactive: "Customer inactive. Please try again or contact our administrator.",
	TokenExpired:     "Token is expired",
	ErrorQuery:       "Error Query",
}

/*
ErrorText returns a text for the error code.
It returns the empty string if the code is unknown.
*/
func ErrorText(code uint) string {
	return errorText[code]
}
