package log

import (
	"context"

	log "github.com/sirupsen/logrus"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type loggerKeyType struct{}

var loggerKey loggerKeyType

func GetLogger(ctx context.Context) *zap.Logger {
	return ctx.Value(loggerKey).(*zap.Logger)
}

func InitLogger(env, service string, options ...zap.Option) *zap.Logger {
	cfg := zap.NewDevelopmentConfig()
	if env == "production" {
		cfg = zap.NewProductionConfig()
	}

	cfg.EncoderConfig.EncodeTime = zapcore.RFC3339TimeEncoder
	logger, _ := cfg.Build(options...)

	logger = logger.With(
		zap.String("server_service_name", service),
	)

	return logger
}

// Info ..
func Info(msg string) {
	log.WithField("msg", msg)
	// log.Info().Msg(msg)
}

// Error ...
func Error(msg string, err error) {
	log.Error(msg, err.Error())
	// log.Error().Str("error", err.Error()).Msg(msg)
}

// Fatal ...
func Fatal(msg string, err error) {
	log.Error(msg, err.Error())
	// log.Fatal().Str("error", err.Error()).Msg(msg)
}

// Panic ...
func Panic(msg string, err error) {
	log.Panic(msg)
	// log.Panic().Msg(msg)
}

// Print ...
func Print(msg string) {
	log.Print(msg)
}
