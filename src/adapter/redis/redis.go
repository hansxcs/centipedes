package redis

import (
	"fmt"
	"os"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/hansxcs/centipedes/src/package/log"
)

// Cache ...
type Cache struct {
}

// New CacheHandler
func New() *Cache {
	return &Cache{}
}

// CacheInterface ...
type CacheInterface interface {
	Ping() (string, error)
	DO(command string, args ...interface{}) (interface{}, error)
	CacheSet(key string, value []byte) (interface{}, error)
	CacheGet(key string) ([]byte, error)
	CacheDel(key string) error
	CacheDelKey(key string) error
	CacheDeletePattern(key string) ([]byte, error)
}

func (handler *Cache) newPool() *redis.Pool {
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")
	password := os.Getenv("REDIS_PASSWORD")
	config := fmt.Sprintf("%s:%s", host, port)
	return &redis.Pool{
		MaxIdle:   80,
		MaxActive: 12000,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", config, redis.DialPassword(password))
			if err != nil {
				log.Fatal("Check redis connection", err)
			}
			return c, err
		},
	}
}

// Ping ...
func (handler *Cache) Ping() (string, error) {
	conn, err := handler.newPool().Dial()
	if err != nil {
		return "", err
	}
	pong, err := conn.Do("PING")
	if err != nil {
		return "", err
	}
	return redis.String(pong, err)
}

// DO ...
func (handler *Cache) DO(command string, args ...interface{}) (interface{}, error) {
	conn, err := handler.newPool().Dial()
	if err != nil {
		return nil, err
	}
	return conn.Do(command, args...)
}

// CacheSet ..
// Expired seconds value
func (handler *Cache) CacheSet(key string, value []byte, expired string) error {

	conn, err := handler.newPool().Dial()
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = conn.Do("SET", key, value, "EX", expired)
	if err != nil {
		v := string(value)
		if len(v) > 15 {
			v = v[0:12] + "..."
		}
		return fmt.Errorf("error setting key %s to %s: %v", key, v, err)
	}
	return err
}

// CacheGet ...
func (handler *Cache) CacheGet(key string) ([]byte, error) {
	conn, err := handler.newPool().Dial()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	var data []byte
	data, err = redis.Bytes(conn.Do("GET", key))
	if err != nil {
		return data, fmt.Errorf("error getting key %s: %v", key, err)
	}
	return data, err
}

// CacheGetPattern ...
func (handler *Cache) CacheGetPattern(pattern string) ([]byte, []string, error) {
	conn, err := handler.newPool().Dial()
	if err != nil {
		return nil, nil, err
	}
	defer conn.Close()

	keys, err := redis.Strings(conn.Do("KEYS", pattern))
	if err != nil {
		log.Error("error get keys ", err)
		return nil, nil, fmt.Errorf("error getting key %s: %v", pattern, err)
	}
	var data []byte
	var dataKeys []string
	for _, v := range keys {
		dataKeys = append(dataKeys, v)
		data, err = redis.Bytes(conn.Do("GET", v))
		if err != nil {
			return data, nil, fmt.Errorf("error getting key %s: %v", v, err)
		}
	}

	return data, dataKeys, err
}

// CacheDel ...
func (handler *Cache) CacheDel(key string) error {
	conn, err := handler.newPool().Dial()
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = redis.Bytes(conn.Do("DEL", key))
	if err != nil {
		return fmt.Errorf("error deleting key %s: %v", key, err)
	}
	return err
}

// CacheDeletePattern ...
func (handler *Cache) CacheDeletePattern(pattern string) error {
	conn, err := handler.newPool().Dial()
	if err != nil {
		return err
	}
	defer conn.Close()

	keys, err := redis.Strings(conn.Do("KEYS", pattern))
	if err != nil {
		log.Error("error get keys ", err)
		return fmt.Errorf("error getting key %s: %v", pattern, err)
	}
	for _, v := range keys {
		_, err := conn.Do("DEL", v)
		if err != nil {
			log.Error("error deleting key ", err)
			return fmt.Errorf("error deleting key %s: %v", v, err)
		}
	}

	return err
}

// CacheDelKey ...
func (handler *Cache) CacheDelKey(key string) error {
	conn, err := handler.newPool().Dial()
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = conn.Do("DEL", key)
	if err != nil {
		return fmt.Errorf("error deleting key %s: %v", key, err)
	}
	return err
}
