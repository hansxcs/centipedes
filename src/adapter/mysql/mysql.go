package mysql

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
)

type MySqlDB struct {
	DB      *sqlx.DB
	Queryer sqlx.Queryer
	Logger  *logrus.Logger
}

// NewMaria initialize single mysql db
func NewMySQL(db *sqlx.DB) *MySqlDB {
	x := MySqlDB{}
	x.DB = db
	x.Queryer = db
	x.Logger = logrus.StandardLogger()
	return &x
}

type Adapter interface {
	QueryRow(query string, args ...interface{}) sql.Result
	// QueryRow(ctx context.Context, query string, args ...interface{}) *sql.Row
	// QueryRows(ctx context.Context, query string, args ...interface{}) (rows *sql.Rows, err error)
	Fetch(dst interface{}, query string, args ...interface{}) error
	FetchRow(dst interface{}, query string, args ...interface{}) error
	// BeginTx(ctx context.Context, opts *sql.TxOptions) (*sql.Tx, error)
	// Exec(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	Ping(ctx context.Context) error
	Query(query string, args ...interface{}) (*sql.Rows, error)
	Queryx(query string, args ...interface{}) (*sqlx.Rows, error)
	QueryRowx(query string, args ...interface{}) *sqlx.Row
	UpdateQuery(table string, data map[string]interface{}) string
}
