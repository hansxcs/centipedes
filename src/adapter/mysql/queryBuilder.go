package mysql

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	"gitlab.com/hansxcs/centipedes/src/utils/helper"
)

// Query sqlx property
func (p *MySqlDB) Query(query string, args ...interface{}) (*sql.Rows, error) {
	p.Logger.Log(logrus.InfoLevel, "during", "query exec", "query", query, "query args", args)
	return p.Queryer.Query(query, args...)
}

// Queryx sqlx property
func (p *MySqlDB) Queryx(query string, args ...interface{}) (*sqlx.Rows, error) {
	p.Logger.Log(logrus.InfoLevel, "during", "query exec", "query", query, "query args", args)
	return p.Queryer.Queryx(query, args...)
}

// QueryRowx sqlx property
func (p *MySqlDB) QueryRowx(query string, args ...interface{}) *sqlx.Row {
	p.Logger.Log(logrus.InfoLevel, "during", "query exec", "query", query, "query args", args)
	return p.Queryer.QueryRowx(query, args...)
}

// Create sqlx property
func (p *MySqlDB) QueryRow(query string, args ...interface{}) sql.Result {
	p.Logger.Log(logrus.InfoLevel, "during", "query exec", "query", query, "query args", args)
	defer helper.TimeTrack(time.Now(), query)
	return p.DB.MustExec(query, args...)
}

// // QueryRow select single row database will return  sql.row raw
// func (d *MySqlDB) QueryRow(ctx context.Context, query string, args ...interface{}) *sql.Row {
// 	d.Logger.Log("during", "query exec", "query", query, "query args", args)
// 	defer helper.TimeTrack(time.Now(), query)
// 	return d.DB.QueryRowContext(ctx, query, args...)
// }

// // QueryRows select multiple rows of database will return  sql.rows raw
// func (d *MySqlDB) QueryRows(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
// 	d.Logger.Log("during", "query exec", "query", query, "query args", args)
// 	defer helper.TimeTrack(time.Now(), query)
// 	return d.DB.QueryContext(ctx, query, args...)
// }

// Fetch select multiple rows of database will cast data to struct passing by parameter
func (d *MySqlDB) Fetch(dst interface{}, query string, args ...interface{}) error {
	defer helper.TimeTrack(time.Now(), query)
	return sqlx.Select(d, dst, query, args...)
}

// FetchRow fetching one row database will cast data to struct passing by parameter
func (d *MySqlDB) FetchRow(dst interface{}, query string, args ...interface{}) error {
	defer helper.TimeTrack(time.Now(), query)
	return sqlx.Get(d, dst, query, args...)
}

// // Exec execute mysql command query
// func (d *MySqlDB) Exec(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
// 	return d.DB.ExecContext(ctx, query, args...)
// }

// // BeginTx start new transaction session
// func (d *MySqlDB) BeginTx(ctx context.Context, opts *sql.TxOptions) (*sql.Tx, error) {
// 	return d.DB.BeginTx(ctx, opts)
// }

// Ping check database connectivity
func (d *MySqlDB) Ping(ctx context.Context) error {
	return d.DB.PingContext(ctx)
}

type NewQueryBuilder struct {
	FullQuery string
}

func (qb *NewQueryBuilder) Select(field string) *NewQueryBuilder {
	array := strings.Split(field, ",")
	var fields string
	for k, v := range array {
		if k == len(array)-1 {
			fields += v
		} else {
			fields += v + ","
		}
	}
	query := fmt.Sprintf("select %s", fields)
	qb.FullQuery += query
	return qb
}

func (qb *NewQueryBuilder) From(table string) *NewQueryBuilder {
	query := fmt.Sprintf(" from %s ", table)
	qb.FullQuery += query
	return qb
}

// arg: users.id,address_users.id
// onExpression: users.id = address_users.id
// JoinTable represents "Join table on users.id = address_users.id"
func (qb *NewQueryBuilder) JoinTable(table string, arg string) *NewQueryBuilder {
	array := strings.Split(arg, ",")
	var onExpression string
	for k, v := range array {
		if k == len(array)-1 {
			onExpression += v + " "
		} else {
			onExpression += v + " = "
		}
	}
	query := fmt.Sprintf(" Join %s ON %s", table, onExpression)
	qb.FullQuery += query
	return qb
}

// value: ari,budiman
// field: name
// OperatorORLike represents "where name in["ari","budiman"]"
func (qb *NewQueryBuilder) OperatorIN(field string, value string) *NewQueryBuilder {
	// turn the value into slice
	array := strings.Split(value, ",")
	// create a slice as size as the value length
	var values = make([]string, 0, len(array))

	for _, v := range array {
		item := fmt.Sprintf("\"%s\"", v)
		values = append(values, item)
	}
	// concat string from slice of value
	query := fmt.Sprintf("where %s IN (%s)", field, strings.Join(values, ","))
	qb.FullQuery += query
	return qb
}

// field: email
// OperatorLike represents "where email LIKE %value%"
func (qb *NewQueryBuilder) OperatorLike(field string, value string) *NewQueryBuilder {
	query := fmt.Sprintf("where %s LIKE \"%%%s%%\"", field, value)
	qb.FullQuery += query
	return qb
}

// field: email
// OperatorEqual represents "where email = value"
func (qb *NewQueryBuilder) OperatorEqual(field string, value interface{}) *NewQueryBuilder {
	query := fmt.Sprintf(" where %s = %v", field, value)
	qb.FullQuery += query
	return qb
}

// field: name,email
// OperatorORLike represents "where name LIKE "%value%" OR email LIKE "%value%""
func (qb *NewQueryBuilder) OperatorORLike(field string, value string) *NewQueryBuilder {
	array := strings.Split(field, ",")
	var query string
	for k, v := range array {
		if k == 0 {
			query += fmt.Sprintf("where %s like \"%%%s%%\" OR ", v, value)
		} else if k == len(array)-1 {
			query += fmt.Sprintf("%s like \"%%%s%%\"", v, value)
		} else {
			query += fmt.Sprintf("%s like \"%%%s%%\" OR ", v, value)
		}
	}
	qb.FullQuery += query
	return qb
}

func (qb *NewQueryBuilder) OrderBy(array []string) *NewQueryBuilder {
	var field string
	for k, v := range array {
		if k == len(array)-1 {
			field += v
		} else {
			field += v + " "
		}
	}
	query := fmt.Sprintf(" ORDER BY %s", field)
	qb.FullQuery += query
	return qb
}

func (qb *NewQueryBuilder) Limit(numb int) *NewQueryBuilder {
	query := fmt.Sprintf(" limit %d ", numb)
	qb.FullQuery += query
	return qb
}

func (qb *NewQueryBuilder) Offset(numb int) *NewQueryBuilder {
	query := fmt.Sprintf(" offset %d ", numb)
	qb.FullQuery += query
	return qb
}

// table: users
// field: "name,created_at,updated_at"
// Create represents "insert into users (name,created_at,updated_at) values (?,?,?)"
func (qb *NewQueryBuilder) Create(table string, field string) *NewQueryBuilder {
	// turn the field into slice
	// "name,created_at,updated_at" => [name created_at updated_at]
	array := strings.Split(field, ",")
	var fields string
	// create prepare statement as size as array
	// length of slice: 3 => ?,?,?
	var values string

	for k, v := range array {
		if k == len(array)-1 {
			fields += v
			values += "?"
		} else {
			fields += v + ", "
			values += "?,"
		}
	}
	query := fmt.Sprintf("insert into %s (%s) values (%s)", table, fields, values)
	qb.FullQuery += query
	return qb
}

// UpdateQuery validate update query string
func (qb *NewQueryBuilder) Update(table string, data map[string]interface{}) *NewQueryBuilder {
	dataValue := ""
	i := 0
	for k, v := range data {
		if _, ok := v.(string); ok {
			dataValue += k + " = '" + v.(string) + "'"
		} else {
			dataValue += k + " = " + v.(string)
		}

		if len(data)-1 != i {
			dataValue += ","
		}
	}

	query := "UPDATE " + table + " SET " + dataValue
	qb.FullQuery += query
	return qb
}

func (qb *NewQueryBuilder) Delete(table string, field string, value interface{}) *NewQueryBuilder {
	query := fmt.Sprintf(" delete from %s where %s = %v", table, field, value)
	qb.FullQuery += query
	return qb
}
