package mysql

import (
	"fmt"
	"os"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/hansxcs/centipedes/src/package/log"
)

// init create new session maria db
func Init() (*sqlx.DB, error) {

	defaultTimezone := os.Getenv("SERVER_TIMEZONE")
	connStr := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=1&loc=%s",
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASS"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_NAME"),
		defaultTimezone,
	)
	db, err := sqlx.Open("mysql", connStr)
	if err != nil {
		log.Fatal("failed to open connection db", err)
		panic(err)
		// return db, err
	}

	db.SetMaxIdleConns(25)
	db.SetMaxOpenConns(20)
	db.SetConnMaxIdleTime(time.Second * 15)
	db.SetConnMaxLifetime(time.Minute * 30)

	return db, nil
}
