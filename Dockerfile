FROM golang:1.15-alpine AS builder

RUN apk update && \
    apk upgrade && \
    apk add curl gcc g++ make git bash --no-cache protoc\ 
    && curl -fLo install.sh https://raw.githubusercontent.com/cosmtrek/air/master/install.sh \
    && chmod +x install.sh && sh install.sh && cp ./bin/air /bin/air \
    && mkdir -p /app 

# Install protoc
ENV PROTOBUF_URL https://github.com/google/protobuf/releases/download/v3.3.0/protobuf-cpp-3.3.0.tar.gz
RUN curl -L -o /tmp/protobuf.tar.gz $PROTOBUF_URL
WORKDIR /tmp/
RUN tar xvzf protobuf.tar.gz
WORKDIR /tmp/protobuf-3.3.0
RUN mkdir -p /export
# RUN ./autogen.sh && \
#     ./configure --prefix=/export && \
#     make -j 3 && \
#     make check && \
#     make install

# Install protoc-gen-go
RUN go get github.com/golang/protobuf/protoc-gen-go
RUN cp -R /go/bin/protoc-gen-go /export/bin
# # Export dependencies
# RUN cp -R /usr/lib/libstdc++* /export/lib
# RUN cp /usr/lib/libgcc_s* /export/lib

WORKDIR /app
COPY    . .

EXPOSE 50051
CMD ["sh", "-c","air"]