[ _if proto_vendor have more than 5 please install Protop for pakcage manager_ ] 

## Protop

Protop https://www.protop.io/about is a package manager for protocol buffers with a mission to simplify the development of gRPC (and anything else that uses protocol buffers).

Without protop, there isn't an intuitive way to mantain protobufs, and this often leads to compromises in how we compose projects. With protop, developers can create cleaner projects in collaborative / distributed contexts and keep their protos small, composable, and totally separate from their implementations.
