package main

import (
	"gitlab.com/hansxcs/centipedes/cmd"
)

func main() {
	cmd.Start()
}
